<?php
/**
 * Header file for the Twenty Twenty WordPress default theme.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

$uri = get_theme_file_uri();

?><!DOCTYPE html>

<html class="no-js" <?php language_attributes(); ?>>

	<head>

		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="<?php bloginfo( 'description' ); ?>">
		<meta name="thumbnail" content="">
		<meta http-equiv="X-UA-Compatible" content="ie=edge">
		<title><?php bloginfo( 'name' ); ?></title>
		<link href="https://fonts.googleapis.com/css2?family=Noto+Sans+JP:wght@300;500;700;900&display=swap" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css2?family=Hind:wght@300;500;700&display=swap" rel="stylesheet">
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
		<link rel="stylesheet" href="<?php echo $uri; ?>/static/assets/css/bootstrap.min.css">
		<link rel="stylesheet" href="https://www.music.ac.jp/asset/css/base.css?v=38">
		<link rel="stylesheet" href="<?php echo $uri; ?>/static/assets/css/style.css">
		<link rel="stylesheet" href="<?php echo $uri; ?>/static/assets/css/animate.css">
		<link rel="alternate" href="<?php echo get_the_permalink(); ?>" hreflang="ja">
		<link rel="canonical" href="<?php echo get_the_permalink(); ?>">

		<?php wp_head(); ?>
		<!-- Google Tag Manager -->
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
			new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
			j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
			'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
			})(window,document,'script','dataLayer','GTM-PV54GS6');</script>
			<!-- End Google Tag Manager -->
		</head>

<body>

<p class="bgUnder"><img src="https://www.music.ac.jp/asset/img/bg_under.jpg" alt="甲陽音楽&amp;ダンス専門学校"></p>
<div id="wrap">

<!-- header -->
<header>
	<div id="bar">
	<p>神戸・甲陽音楽&amp;ダンス専門学校 0120-117-540　<a href="http://line.naver.jp/ti/p/%40ady1027z" target="_blank"><i class="fab fa-line"></i></a> <a href="https://www.instagram.com/kobe_koyo/" target="_blank"><i class="fab fa-instagram"></i></a> <a href="https://twitter.com/kobe_ksm" target="_blank"><i class="fab fa-twitter-square"></i></a> <a href="https://www.youtube.com/channel/UCvsu6DqzhnHkFxXpbekhuzA" target="_blank"><i class="fab fa-youtube-square"></i></a>
	</p>
	</div>
	<p id="logo"><a href="https://www.music.ac.jp/"><img src="https://www.music.ac.jp/asset/img/f_logo.png" alt="" class="img d-inline"></a></p>


	<p class="menu">
	<a href="https://www.music.ac.jp/about/" id="show1">KOYOについて <i class="fas fa-angle-down"></i></a>　　
	<a href="https://www.music.ac.jp/course/" id="show2">コース <i class="fas fa-angle-down"></i></a>　　
	<a href="https://www.music.ac.jp/jazz/" id="show7">JAZZ <i class="fas fa-angle-down"></i></a>　　
	<a href="https://www.music.ac.jp/debut/" id="show3">デビュー・就職 <i class="fas fa-angle-down"></i></a>　　
	<a href="https://www.music.ac.jp/oversea/" id="show4">海外で学ぶ <i class="fas fa-angle-down"></i></a>　　
	<a href="https://www.music.ac.jp/schoollife/" id="show5">学園生活 <i class="fas fa-angle-down"></i></a>　　
	<a href="https://www.music.ac.jp/application_requirements/" id="show6">入学案内 <i class="fas fa-angle-down"></i></a>　　

	<a href="https://www.music.ac.jp/event/">オープンキャンパス</a>　　
	<a href="https://www.music.ac.jp/highschool/" target="_blank">高等専修学校 <i class="far fa-window-restore"></i></a>　　
	</p>




	<div class="menuT">

	<div id="wshow1" style="display: none;">
	<ul>
	<li><a href="https://www.music.ac.jp/about/internationaleducation.html">世界基準を学ぶ</a></li>
	<li><a href="https://www.music.ac.jp/about/berklee.html">バークリー音楽大学 提携校</a></li>
	<li><a href="https://www.music.ac.jp/about/access.html">アクセス</a></li>
	<li><a href="https://www.music.ac.jp/about/facility.html" target="_blank">フロアガイド <i class="far fa-window-restore"></i></a></li>
	</ul>
	</div>


	<div id="wshow2" style="display: none; height: 615px; padding-top: 0px; margin-top: 0px; padding-bottom: 0px; margin-bottom: 0px;">
	<p><a href="https://www.music.ac.jp/course/music/musician.html">MUSIC  <i class="fas fa-angle-down"></i></a></p>

	<ul>
	<li><a href="https://www.music.ac.jp/course/music/musician.html">海外音楽留学</a></li>
	<li><a href="https://www.music.ac.jp/course/music/jazz.html">JAZZミュージシャン</a></li>
	<li><a href="https://www.music.ac.jp/course/music/vocal.html">ヴォーカル</a></li>
	<li><a href="https://www.music.ac.jp/course/music/professional.html">ミュージシャン</a></li>
	<li><a href="https://www.music.ac.jp/course/music/business.html">コンサートスタッフ</a></li>
	<li><a href="https://www.music.ac.jp/course/music/recording.html">DJ・作曲・DTM</a></li>
	<li><a href="https://www.music.ac.jp/course/music/e-sports.html">e-sports</a></li>
	<li><a href="https://www.music.ac.jp/course/music/application.html">スマホアプリクリエイター</a></li>

	</ul>





	<p class="pdTop2"><a href="chttps://www.music.ac.jp/course/dance/abroad.html">DANCE <i class="fas fa-angle-down"></i></a></p>




	<ul>
	<li><a href="https://www.music.ac.jp/course/dance/abroad.html">海外ダンス留学</a></li>
	<li><a href="https://www.music.ac.jp/course/dance/dance.html">ダンス</a></li>
	<li><a href="https://www.music.ac.jp/course/dance/actor.html">俳優・声優</a></li>
	</ul>
	<p><a href="https://www.music.ac.jp/course/graduates.html">卒業生インタビュー</a></p>
	<p><a href="https://www.music.ac.jp/course/instructors.html">講師紹介</a></p>
	</div>


	<div id="wshow3" style="display: none;">
	<ul>
	<li><a href="https://www.music.ac.jp/debut/debutsystem.html">デビューシステム</a></li>
	<li><a href="https://www.music.ac.jp/debut/educationsystem.html">企業プロジェクト</a></li>
	<li><a href="https://www.music.ac.jp/debut/employmentsystem.html">就職システム</a></li>
	</ul>
	</div>


	<div id="wshow4" style="display: none; height: 164px; padding-top: 0px; margin-top: 0px; padding-bottom: 0px; margin-bottom: 0px;">
	<ul>
	<li><a href="https://www.music.ac.jp/oversea/graduate.html">海外で学ぶ卒業生</a></li>
	<li><a href="https://www.music.ac.jp/oversea/studyabroad.html">海外留学の流れ</a></li>
	<li><a href="https://www.music.ac.jp/oversea/support-music.html">留学サポート［音楽］</a></li>
	<li><a href="https://www.music.ac.jp/oversea/support-dance.html">留学サポート［ダンス］</a></li>
	</ul>
	</div>


	<div id="wshow5" style="display: none; height: 82px; padding-top: 0px; margin-top: 0px; padding-bottom: 0px; margin-bottom: 0px;">
	<ul>
	<li><a href="https://www.music.ac.jp/schoollife/dormitory.html">学生寮・ひとり暮らし</a></li>
	<li><a href="https://www.music.ac.jp/about/facility.html">フロアガイド <i class="far fa-window-restore"></i></a></li>
	</ul>
	</div>


	<div id="wshow6" style="display: none;">
	<ul>
	<li><a href="https://www.music.ac.jp/application_requirements/eligibility.html">入学資格・出願方法</a></li>
	<li><a href="https://www.music.ac.jp/application_requirements/ao.html">AO入学について</a></li>
	<li><a href="https://www.music.ac.jp/application_requirements/aoapplication.html">ネット出願AO入学</a></li>
	<li><a href="https://www.music.ac.jp/application_requirements/society.html">社会人入学について</a></li>
	<li><a href="https://www.music.ac.jp/application_requirements/recommended.html">推薦入学について</a></li>
	<li><a href="https://www.music.ac.jp/application_requirements/general.html">一般入学について</a></li>
	<li><a href="https://www.music.ac.jp/application_requirements/flow.html">出願から合格まで</a></li>
	<li><a href="https://www.music.ac.jp/application_requirements/pdf/howto.pdf" target="_blank">出願書類記入方法<i class="far fa-file-pdf"></i></a></li>
	<li><a href="https://www.music.ac.jp/application_requirements/scholarship.html">学費サポート</a></li>
	</ul>
	</div>


	<div id="wshow7" style="display: none;">
	<ul>
	<li><a href="https://www.music.ac.jp/jazz/graduate.html">卒業生インタビュー</a></li>
	<li><a href="https://www.music.ac.jp/jazz/kobe.html">神戸でジャズを学ぶ</a></li>
	<li><a href="https://www.music.ac.jp/jazz/instructors.html">ジャズ講師</a></li>

	</ul>
	</div>



	</div>




	<p id="jkk"><a href="https://www.music.ac.jp/public_info/">学校情報公開</a></p>
	<p id="status"><a href="#">訪問者別メニュー <i class="fas fa-angle-down"></i></a></p>

	<div id="statusMenu">
	<ul>
	<li><a href="https://www.music.ac.jp/visitor/applicant.html">入学希望の方へ</a></li>
	<li><a href="https://www.music.ac.jp/visitor/student.html">在校生の方へ</a></li>
	<li><a href="https://www.music.ac.jp/visitor/alumni.html">卒業生の方へ</a></li>
	<li><a href="https://www.music.ac.jp/visitor/parents.html">保護者の方へ</a></li>
	<li><a href="https://www.music.ac.jp/visitor/abroad.html">留学生の方へ</a></li>
	<li><a href="https://www.music.ac.jp/visitor/company.html">企業の方へ</a></li>
	</ul>
	</div>
	<link href="https://fonts.googleapis.com/css?family=Raleway:400,500,600,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Noto+Sans+JP:400,700&amp;subset=japanese" rel="stylesheet">
</header>
<!-- header -->

<!-- menu-icon -->
<div id="mTrigger">
	<p></p>
	<p></p>
	<p></p>
	<p>メニュー</p>
</div>
<!-- menu-icon -->

<!-- menu -->
<div id="spMenu" style="/* visibility: hidden; *//* opacity: 0; */">

	<div>

	<p class="logo" style="box-sizing: content-box;"><a href="https://www.music.ac.jp/"><img src="https://www.music.ac.jp/asset/img/f_logo.png" alt="" width="78%" height="auto"></a></p>

	<h3 class="bt">KOYOについて <i class="fas fa-plus"></i></h3>
	<ul class="drawer">
	<li><a href="https://www.music.ac.jp/about/">KOYOについて <i class="fas fa-angle-right"></i></a></li>
	<li><a href="https://www.music.ac.jp/about/internationaleducation.html">世界基準を学ぶ <i class="fas fa-angle-right"></i></a></li>
	<li><a href="https://www.music.ac.jp/about/berklee.html">バークリー音楽大学 提携校 <i class="fas fa-angle-right"></i></a></li>
	<li><a href="https://www.music.ac.jp/about/access.html">アクセス <i class="fas fa-angle-right"></i></a></li>
	<li><a href="https://www.music.ac.jp/about/facility.html" target="_blank">フロアガイド <i class="far fa-window-restore"></i></a></li>
	</ul>

	<h3>JAZZ<i class="fas fa-plus"></i></h3>
	<ul class="drawer">
	<li><a href="https://www.music.ac.jp/jazz/">JAZZ <i class="fas fa-angle-right"></i></a></li>
	<li><a href="https://www.music.ac.jp/jazz/graduate.html">卒業生インタビュー <i class="fas fa-angle-right"></i></a></li>
	<li><a href="https://www.music.ac.jp/jazz/kobe.html">神戸でジャズを学ぶ <i class="fas fa-angle-right"></i></a></li>
	<li><a href="https://www.music.ac.jp/jazz/instructors.html">ジャズ講師 <i class="fas fa-angle-right"></i></a></li>

	</ul>

	<h3>コース <i class="fas fa-plus"></i></h3>
	<ul class="drawer">
	<li><a href="https://www.music.ac.jp/course/">コース一覧 <i class="fas fa-angle-right"></i></a></li>
	<li><a href="https://www.music.ac.jp/course/music/musician.html">プロミュージシャン科<br>海外音楽留学<i class="fas fa-angle-right"></i></a></li>
	<li><a href="https://www.music.ac.jp/course/music/jazz.html">プロミュージシャン科<br>JAZZミュージシャン<i class="fas fa-angle-right"></i></a></li>
	<li><a href="https://www.music.ac.jp/course/music/vocal.html">プロミュージシャン科<br>ヴォーカル<i class="fas fa-angle-right"></i></a></li>
	<li><a href="https://www.music.ac.jp/course/music/professional.html">プロミュージシャン科<br>ミュージシャン<i class="fas fa-angle-right"></i></a></li>
	<li><a href="https://www.music.ac.jp/course/music/business.html">商業音楽科<br>コンサートスタッフ<i class="fas fa-angle-right"></i></a></li>
	<li><a href="https://www.music.ac.jp/course/music/recording.html">音楽クリエーター科<br>DJ・作曲・DTM<i class="fas fa-angle-right"></i></a></li>
	<li><a href="https://www.music.ac.jp/course/music/e-sports.html">音楽クリエーター科<br>e-sports<i class="fas fa-angle-right"></i></a></li>
	<li><a href="https://www.music.ac.jp/course/music/application.html">音楽クリエーター科<br>スマホアプリクリエイター<i class="fas fa-angle-right"></i></a></li>
	<li><a href="https://www.music.ac.jp/course/dance/abroad.html">ダンス＆アクターズ科<br>海外ダンス留学<i class="fas fa-angle-right"></i></a></li>
	<li><a href="https://www.music.ac.jp/course/dance/dance.html">ダンス＆アクターズ科<br>ダンス<i class="fas fa-angle-right"></i></a></li>
	<li><a href="https://www.music.ac.jp/course/dance/actor.html">ダンス＆アクターズ科<br>俳優・声優<i class="fas fa-angle-right"></i></a></li>
	<li><a href="https://www.music.ac.jp/course/graduates.html">卒業生インタビュー<i class="fas fa-angle-right"></i></a></li>
	<li><a href="https://www.music.ac.jp/course/instructors.html">講師紹介<i class="fas fa-angle-right"></i></a></li>
	</ul>


	<h3>デビュー・就職 <i class="fas fa-plus"></i></h3>

	<ul class="drawer">
	<li><a href="https://www.music.ac.jp/debut/">デビュー・就職  <i class="fas fa-angle-right"></i></a></li>
	<li><a href="https://www.music.ac.jp/debut/debutsystem.html">デビューシステム <i class="fas fa-angle-right"></i></a></li>
	<li><a href="https://www.music.ac.jp/debut/educationsystem.html">企業プロジェクト <i class="fas fa-angle-right"></i></a></li>
	<li><a href="https://www.music.ac.jp/debut/employmentsystem.html">就職システム <i class="fas fa-angle-right"></i></a></li>
	</ul>


	<h3>海外で学ぶ <i class="fas fa-plus"></i></h3>
	<ul class="drawer">
	<li><a href="https://www.music.ac.jp/oversea/">海外で学ぶ <i class="fas fa-angle-right"></i></a></li>
	<li><a href="https://www.music.ac.jp/oversea/studyabroad.html">海外留学の流れ <i class="fas fa-angle-right"></i></a></li>
	<li><a href="https://www.music.ac.jp/oversea/support-music.html">留学サポート［音楽］ <i class="fas fa-angle-right"></i></a></li>
	<li><a href="https://www.music.ac.jp/oversea/support-dance.html">留学サポート［ダンス］ <i class="fas fa-angle-right"></i></a></li>	
	</ul>


	<h3>学園生活 <i class="fas fa-plus"></i></h3>
	<ul class="drawer">
	<li><a href="https://www.music.ac.jp/schoollife/dormitory.html">学生寮・ひとり暮らし <i class="fas fa-angle-right"></i></a></li>
	<li><a href="https://www.music.ac.jp/schoollife/">学園生活 <i class="fas fa-angle-right"></i></a></li>
	</ul>

	<h3>入学案内 <i class="fas fa-plus"></i></h3>
	<ul class="drawer">
	<li><a href="https://www.music.ac.jp/application_requirements/">入学案内 <i class="fas fa-angle-right"></i></a></li>
	<li><a href="https://www.music.ac.jp/application_requirements/eligibility.html">入学資格・出願方法 <i class="fas fa-angle-right"></i></a></li>
	<li><a href="https://www.music.ac.jp/application_requirements/ao.html">AO入学について <i class="fas fa-angle-right"></i></a></li>
	<li><a href="https://www.music.ac.jp/application_requirements/aoapplication.html">ネット出願AO入学 <i class="fas fa-angle-right"></i></a></li>
	<li><a href="https://www.music.ac.jp/application_requirements/society.html">社会人入学について <i class="fas fa-angle-right"></i></a></li>
	<li><a href="https://www.music.ac.jp/application_requirements/recommended.html">推薦入学について <i class="fas fa-angle-right"></i></a></li>
	<li><a href="https://www.music.ac.jp/application_requirements/general.html">一般入学について <i class="fas fa-angle-right"></i></a></li>
	<li><a href="https://www.music.ac.jp/application_requirements/flow.html">出願から合格まで <i class="fas fa-angle-right"></i></a></li>
	<li><a href="https://www.music.ac.jp/application_requirements/pdf/howto.pdf" target="_blank">出願書類記入方法<i class="far fa-file-pdf"></i></a></li>
	<li><a href="https://www.music.ac.jp/application_requirements/scholarship.html">学費サポート <i class="fas fa-angle-right"></i></a></li>
	</ul>

	<h3>募集要項 <i class="fas fa-plus"></i></h3>
	<ul class="drawer">
	<li><a href="https://www.music.ac.jp/application_requirements/">募集要項 <i class="fas fa-angle-right"></i></a></li>
	<li><a href="https://www.music.ac.jp/application_requirements/eligibility.html">入学資格・出願方法 <i class="fas fa-angle-right"></i></a></li>
	<li><a href="https://www.music.ac.jp/application_requirements/ao.html">AO入学について <i class="fas fa-angle-right"></i></a></li>
	<li><a href="https://www.music.ac.jp/application_requirements/aoapplication.html">ネット出願AO入学 <i class="fas fa-angle-right"></i></a></li>
	<li><a href="https://www.music.ac.jp/application_requirements/society.html">社会人入学について <i class="fas fa-angle-right"></i></a></li>
	<li><a href="https://www.music.ac.jp/application_requirements/recommended.html">推薦入学について <i class="fas fa-angle-right"></i></a></li>
	<li><a href="https://www.music.ac.jp/application_requirements/general.html">一般入学について <i class="fas fa-angle-right"></i></a></li>
	<li><a href="https://www.music.ac.jp/application_requirements/flow/">出願から合格まで <i class="fas fa-angle-right"></i></a></li>
	<li><a href="https://www.music.ac.jp/application_requirements/pdf/howto.pdf" target="_blank">出願書類記入方法<i class="far fa-file-pdf"></i></a></li>
	<li><a href="https://www.music.ac.jp/application_requirements/scholarship.html">学費サポート <i class="fas fa-angle-right"></i></a></li>
	</ul>


	<h3 class="dm"><a href="https://www.music.ac.jp/event/">オープンキャンパス <i class="fas fa-calendar-alt"></i></a></h3>
	<h3 class="dm"><a href="https://www.music.ac.jp/event/form.html?form_code=1298" target="_blank">資料請求はこちら <i class="fas fa-book-open"></i></a></h3>

	</div>
	
</div>
<!-- menu -->

<!-- cv -->
<div id="t12">
	<ul>
	<li><a href="https://www.music.ac.jp/event/"><img src="https://www.music.ac.jp/asset/img/bnr_01.jpg" width="100%" height="auto" alt="オープンキャンパス" class="pc"><img src="https://www.music.ac.jp/asset/img/bnr_01_sp.jpg" width="100%" height="auto" alt="オープンキャンパス" class="sp"></a></li>
	<li><a href="https://www.music.ac.jp/event/form.html?form_code=1298" target="_blank"><img src="https://www.music.ac.jp/asset/img/bnr_02.jpg?v=1" width="100%" height="auto" alt="資料請求" class="pc"><img src="https://www.music.ac.jp/asset/img/bnr_02_sp.jpg?v=1" width="100%" height="auto" alt="資料請求" class="sp"></a></li>
	</ul>
</div>
<!-- cv -->