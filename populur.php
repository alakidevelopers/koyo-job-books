<?php
    $uri = get_theme_file_uri();
?>
<section class="sec other-archive">
    <div class="inner">
        <div class="sec-ttlarea color-white layer">
            <div class="sec-enttl">Popular jobs</div>
            <h2 class="sec-ttl">高校生が<br class="d-md-none">よく見ているお仕事</h2>
        </div><!-- .sec-ttlarea -->
        <div class="card-wrap row">
            <?php
                $ranks = array('first','second','third');
                $count = 1;
                foreach($ranks as $rank):

                $args = array(
                    'posts_per_page' => 1,
                    'meta_key' => 'populur',
                    'meta_value' => $rank
                );
                $the_query = new WP_Query( $args );
                if ( $the_query->have_posts() ) :
                while ( $the_query->have_posts() ) : $the_query->the_post();

                // 記事のカテゴリー情報を取得する
                $cat = get_the_category();

                // 取得した配列から必要な情報を変数に入れる
                $cat_name = $cat[0]->cat_name; // カテゴリー名
                $cat_slug = $cat[0]->category_nicename; // カテゴリースラッグ
                $cat_id = $cat[0]->cat_ID; // カテゴリーID
                $cat_initial = get_field( 'initial', 'category_' . $cat_id );
                $cat_num = get_field( 'num', 'category_' . $cat_id );
                // var_dump(get_post_meta($post->ID,'populur'));
            ?>
            <div class="card col-lg-4">
                <div class="card-badge" style="z-index:2;"><img src="<?php echo $uri; ?>/static/assets/images/rank<?php echo $count; ?>.svg" alt="1"></div>
                <picture class="link-area" style="min-height: 0%;">
                    <a href="<?php the_permalink(); ?>">
                    <?php if (has_post_thumbnail()) { //アイキャッチ画像を設定している場合
                        the_post_thumbnail('full');
                        } else { //アイキャッチ画像を設定していない場合 ?>
                        <img src="<?php echo $uri; ?>/static/assets/images/dammy@2x.png" />
                    <?php } ?>
                    </a>
                </picture>
                <div class="card-body has-btn">
                    <div class="tag-cloud mb-3">
                        <a href="<?php echo home_url('/') . $cat_slug; ?>/" class="tag" data-color="<?php echo $cat_initial;?>"><?php echo $cat_name; ?>のお仕事</a>
                    </div>
                    <h3 class="card-title"><a href="<?php the_permalink(); ?>" class="color-<?php echo $cat_initial; ?>"><?php echo get_the_title(); ?></a></h3>
                    <div class="card-text sentence">
                        <p>
                            <?php
                                echo mb_substr(get_field( 'when_contents' ),0,100)."...";
                            ?>
                        </p>
                    </div>
                    <a href="<?php the_permalink(); ?>" class="btn mt-5" data-color="<?php echo $cat_initial;?>"><?php echo get_the_title(); ?>のお仕事を見る</a>
                </div>
            </div><!-- .card -->
            <?php
                $count++;
                endwhile;
                endif;

                endforeach;
                wp_reset_postdata();
            ?>
        </div><!-- .card-wrap -->
    </div><!-- .inner -->
</section><!-- .sec -->