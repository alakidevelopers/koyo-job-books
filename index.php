<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

$media_pc = "(min-width: 991px)";
$uri = get_theme_file_uri();

get_header();
?>

<main class="main frontpage">
	<div class="fv">
		<picture>
			<source srcset="<?php echo $uri; ?>/static/assets/images/job_books_mainimg_pc@2x.png" media="<?php echo $media_pc; ?>"/>
			<img src="<?php echo $uri; ?>/static/assets/images/job_books_mainimg@2x.png" /><!-- それ以外で表示 -->
		</picture>
		<h1 class="fv-logo" itemscope itemtype="http://schema.org/Organization">
			<a itemprop="url" href="/"><img itemprop="logo" src="<?php echo $uri; ?>/static/assets/images/job_books_logo.svg" /></a>
		</h1>
		<nav class="breadcrumb">
			<ol class="d-flex" itemscope itemtype="http://schema.org/BreadcrumbList">
				<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
					<a itemprop="item" href="https://www.music.ac.jp/">
						<span itemprop="name"><i class="fas fa-home"></i>トップ<i class="fas fa-chevron-right arrow"></i></span></a>
					<meta itemprop="position" content="1" />
				</li>
				<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
					<div itemprop="item" href="https://example.com/arts/books">
						<span itemprop="name">お仕事図鑑</span>
					</div>
					<meta itemprop="position" content="2" />
				</li>
			</ol>
		</nav><!-- .breadcrumb -->
	</div>
	<div class="intro pt-0 shadow-btm pb-4">
		<div class="sec-ttlarea color-white layer">
			<h2 class="sec-ttl">神戸・甲陽でめざせる<br>音楽・エンターテイメント<br class="d-md-none">のお仕事</h2>
			<p class="font-weight-bold">世の中にはたくさんの仕事があります。<br>
			キミの「好き」を「仕事」にできる<br class="d-md-none">
			職業を見つけよう！</p>
		</div><!-- .sec-ttlarea -->
	</div>
	<article>
		<section class="sec pt-0 bg-gray cat-archive">
			<div class="inner-full">
				<div class="sec-body row">
					<div class="card-wrap col-lg-6">

						<!-- ヴォーカリスト・ミュージシャン -->
						<div class="card card-wide">
							<picture>
								<img src="<?php echo $uri; ?>/static/assets/images/job_01_mainimg@2x.png" /><!-- それ以外で表示 -->
							</picture>
							<div class="card-body position-relative">
								<h3 class="card-title layer color-vm">ヴォーカリスト・<br>ミュージシャン<span class="color-black fz-16 font-weight-bold">のお仕事</span></h5>
								<figure class="cat-icon"><img src="<?php echo $uri; ?>/static/assets/images/job_01_icon.svg" /></figure>
								<div class="btn-list layer">
									<?php
										$args = array(
											'category_name' => 'vocalist_musician',
											'posts_per_page' => -1
										);
										$the_query = new WP_Query( $args );
										if ( $the_query->have_posts() ) :
										while ( $the_query->have_posts() ) : $the_query->the_post();
									?>
									<a href="<?php the_permalink(); ?>" class="btn btn-large btn-full btn-border-left text-left mt-0" data-color="vm"><?php echo get_the_title(); ?></a>
									<?php
										endwhile;
										endif;
										wp_reset_postdata();
									?>
								</div>
								<div class="text-center">
									<a href="<?php echo home_url(); ?>/vocalist_musician/" class="btn btn-line" data-color="vm">一覧を見る</a>
								</div>
							</div>
						</div><!-- .card -->

						<!-- ライブ・イベント・コンサート -->
						<div class="card card-wide">
							<picture>
								<img src="<?php echo $uri; ?>/static/assets/images/job_07_mainimg@2x.png" /><!-- それ以外で表示 -->
							</picture>
							<div class="card-body position-relative">
								<h3 class="card-title layer color-lec">ライブ・イベント・<br>コンサート<span class="color-black fz-16 font-weight-bold">のお仕事</span></h5>
								<figure class="cat-icon"><img src="<?php echo $uri; ?>/static/assets/images/job_07_icon.svg" /></figure>
								<div class="btn-list layer">
									<?php
										$args = array(
											'category_name' => 'live_event_concert',
											'posts_per_page' => -1
										);
										$the_query = new WP_Query( $args );
										if ( $the_query->have_posts() ) :
										while ( $the_query->have_posts() ) : $the_query->the_post();
									?>
									<a href="<?php the_permalink(); ?>" class="btn btn-large btn-full btn-border-left text-left mt-0" data-color="lec"><?php echo get_the_title(); ?></a>
									<?php
										endwhile;
										endif;
										wp_reset_postdata();
									?>
								</div>
								<div class="text-center">
									<a href="<?php echo home_url(); ?>/live_event_concert/" class="btn btn-line" data-color="lec">一覧を見る</a>
								</div>
							</div>
						</div><!-- .card -->

						<!-- 音楽ビジネス・マネージメント -->
						<div class="card card-wide">
							<picture>
								<img src="<?php echo $uri; ?>/static/assets/images/job_03_mainimg@2x.png" /><!-- それ以外で表示 -->
							</picture>
							<div class="card-body position-relative">
								<h3 class="card-title layer color-bm">音楽ビジネス・<br>マネージメント<span class="color-black fz-16 font-weight-bold">のお仕事</span></h5>
								<figure class="cat-icon"><img src="<?php echo $uri; ?>/static/assets/images/job_03_icon.svg" /></figure>
								<div class="btn-list layer">
									<?php
										$args = array(
											'category_name' => 'music-business_management',
											'posts_per_page' => -1
										);
										$the_query = new WP_Query( $args );
										if ( $the_query->have_posts() ) :
										while ( $the_query->have_posts() ) : $the_query->the_post();
									?>
									<a href="<?php the_permalink(); ?>" class="btn btn-large btn-full btn-border-left text-left mt-0" data-color="bm"><?php echo get_the_title(); ?></a>
									<?php
										endwhile;
										endif;
										wp_reset_postdata();
									?>
								</div>
								<div class="text-center">
									<a href="<?php echo home_url(); ?>/music-business_management/" class="btn btn-line" data-color="bm">一覧を見る</a>
								</div>
							</div>
						</div><!-- .card -->

						<!-- ダンサー -->
						<div class="card card-wide">
							<picture>
								<img src="<?php echo $uri; ?>/static/assets/images/job_06_mainimg@2x.png" /><!-- それ以外で表示 -->
							</picture>
							<div class="card-body position-relative">
								<h3 class="card-title layer color-d">ダンサー<span class="color-black fz-16 font-weight-bold">のお仕事</span></h5>
								<figure class="cat-icon"><img src="<?php echo $uri; ?>/static/assets/images/job_06_icon.svg" /></figure>
								<div class="btn-list layer">
									<?php
										$args = array(
											'category_name' => 'dancer',
											'posts_per_page' => -1
										);
										$the_query = new WP_Query( $args );
										if ( $the_query->have_posts() ) :
										while ( $the_query->have_posts() ) : $the_query->the_post();
									?>
									<a href="<?php the_permalink(); ?>" class="btn btn-large btn-full btn-border-left text-left mt-0" data-color="d"><?php echo get_the_title(); ?></a>
									<?php
										endwhile;
										endif;
										wp_reset_postdata();
									?>
								</div>
								<div class="text-center">
									<a href="<?php echo home_url(); ?>/dancer/" class="btn btn-line" data-color="d">一覧を見る</a>
								</div>
							</div>
						</div><!-- .card -->

					</div><!-- .card-wrap -->

					<div class="card-wrap col-lg-6">

						<!-- JAZZミュージシャン -->
						<div class="card card-wide">
							<picture>
								<img src="<?php echo $uri; ?>/static/assets/images/job_05_mainimg@2x.png" /><!-- それ以外で表示 -->
							</picture>
							<div class="card-body position-relative">
								<h3 class="card-title layer color-jm">JAZZミュージシャン<span class="color-black fz-16 font-weight-bold">のお仕事</span></h5>
								<figure class="cat-icon"><img src="<?php echo $uri; ?>/static/assets/images/job_05_icon.svg" /></figure>
								<div class="btn-list layer">
									<?php
										$args = array(
											'category_name' => 'jazz-musician',
											'posts_per_page' => -1
										);
										$the_query = new WP_Query( $args );
										if ( $the_query->have_posts() ) :
										while ( $the_query->have_posts() ) : $the_query->the_post();
									?>
									<a href="<?php the_permalink(); ?>" class="btn btn-large btn-full btn-border-left text-left mt-0" data-color="jm"><?php echo get_the_title(); ?></a>
									<?php
										endwhile;
										endif;
										wp_reset_postdata();
									?>
								</div>
								<div class="text-center">
									<a href="<?php echo home_url(); ?>/jazz-musician/" class="btn btn-line" data-color="jm">一覧を見る</a>
								</div>
							</div>
						</div><!-- .card -->

						<!-- 俳優・タレント・声優 -->
						<div class="card card-wide">
							<picture>
								<img src="<?php echo $uri; ?>/static/assets/images/job_04_mainimg@2x.png" /><!-- それ以外で表示 -->
							</picture>
							<div class="card-body position-relative">
								<h3 class="card-title layer color-atv">俳優・タレント・声優<span class="color-black fz-16 font-weight-bold">のお仕事</span></h5>
								<figure class="cat-icon"><img src="<?php echo $uri; ?>/static/assets/images/job_04_icon.svg" /></figure>
								<div class="btn-list layer">
									<?php
										$args = array(
											'category_name' => 'actor_talent_voice-actor',
											'posts_per_page' => -1
										);
										$the_query = new WP_Query( $args );
										if ( $the_query->have_posts() ) :
										while ( $the_query->have_posts() ) : $the_query->the_post();
									?>
									<a href="<?php the_permalink(); ?>" class="btn btn-large btn-full btn-border-left text-left mt-0" data-color="atv"><?php echo get_the_title(); ?></a>
									<?php
										endwhile;
										endif;
										wp_reset_postdata();
									?>
								</div>
								<div class="text-center">
									<a href="<?php echo home_url(); ?>/actor_talent_voice-actor/" class="btn btn-line" data-color="atv">一覧を見る</a>
								</div>
							</div>
						</div><!-- .card -->

						<!-- プロデューサー・音楽クリエーター -->
						<div class="card card-wide">
							<picture>
								<img src="<?php echo $uri; ?>/static/assets/images/job_08_mainimg@2x.png" /><!-- それ以外で表示 -->
							</picture>
							<div class="card-body position-relative">
								<h3 class="card-title layer color-pm">プロデューサー・<br>音楽クリエーター<span class="color-black fz-16 font-weight-bold">のお仕事</span></h5>
								<figure class="cat-icon"><img src="<?php echo $uri; ?>/static/assets/images/job_08_icon.svg" /></figure>
								<div class="btn-list layer">
									<?php
										$args = array(
											'category_name' => 'producer_music-creator',
											'posts_per_page' => -1
										);
										$the_query = new WP_Query( $args );
										if ( $the_query->have_posts() ) :
										while ( $the_query->have_posts() ) : $the_query->the_post();
									?>
									<a href="<?php the_permalink(); ?>" class="btn btn-large btn-full btn-border-left text-left mt-0" data-color="pm"><?php echo get_the_title(); ?></a>
									<?php
										endwhile;
										endif;
										wp_reset_postdata();
									?>
								</div>
								<div class="text-center">
									<a href="<?php echo home_url(); ?>/producer_music-creator/" class="btn btn-line" data-color="pm">一覧を見る</a>
								</div>
							</div>
						</div><!-- .card -->

						<!-- e-sports・スマホアプリ・映像 -->
						<div class="card card-wide">
							<picture>
								<img src="<?php echo $uri; ?>/static/assets/images/job_02_mainimg@2x.png" /><!-- それ以外で表示 -->
							</picture>
							<div class="card-body position-relative">
								<h3 class="card-title layer color-esm">e-sports・スマホアプリ・<br>映像<span class="color-black fz-16 font-weight-bold">のお仕事</span></h5>
								<figure class="cat-icon"><img src="<?php echo $uri; ?>/static/assets/images/job_02_icon.svg" /></figure>
								<div class="btn-list layer">
									<?php
										$args = array(
											'category_name' => 'e-sports_application_video',
											'posts_per_page' => -1
										);
										$the_query = new WP_Query( $args );
										if ( $the_query->have_posts() ) :
										while ( $the_query->have_posts() ) : $the_query->the_post();
									?>
									<a href="<?php the_permalink(); ?>" class="btn btn-large btn-full btn-border-left text-left mt-0" data-color="esm"><?php echo get_the_title(); ?></a>
									<?php
										endwhile;
										endif;
										wp_reset_postdata();
									?>
								</div>
								<div class="text-center">
									<a href="<?php echo home_url(); ?>/e-sports_application_video/" class="btn btn-line" data-color="esm">一覧を見る</a>
								</div>
							</div>
						</div><!-- .card -->
					</div><!-- .card-wrap -->
				</div><!-- .sec-body -->
			</div>
		</section><!-- .sec -->

		<?php get_template_part('populur'); ?>

	</article>

</main><!-- .main -->

<?php
get_footer();
