<?php
/**
 * The template for displaying single posts and pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

$media_pc = "(min-width: 991px)";

$uri = get_theme_file_uri();

$post_name = get_the_title();

$cat = get_the_category();

// 取得した配列から必要な情報を変数に入れる
$cat_name = $cat[0]->cat_name; // カテゴリー名
$cat_slug = $cat[0]->category_nicename; // カテゴリースラッグ
$cat_id = $cat[0]->cat_ID; // カテゴリーID
$cat_initial = get_field( 'initial', 'category_' . $cat_id );
$cat_num = get_field( 'num', 'category_' . $cat_id );

$fileds = array(
	'when'=>array('when_contents','when_image','とは?'),
	'job_contents'=>array('job_contents','job_image','の仕事内容'),
	'rewarding'=>array('rewarding_contents','rewarding_image','のやりがい・楽しさ'),
	'become'=>array('become_contents','become_image','になるにはどうすればいいの？'),
	'qualification'=>array('qualification_contents','qualification_image','になるために必要な能力・資格'),
	'message'=>array('message_contents','message_image','になりたい高校生へのメッセージ')
);

get_header();
?>

<main class="main singular page">
	<div class="fv">
		<picture>
			<source srcset="<?php echo $uri; ?>/static/assets/images/job_books_mainimg_page_pc@2x.png" media="<?php echo $media_pc; ?>"/>
			<img src="<?php echo $uri; ?>/static/assets/images/job_books_mainimg_page@2x.png" /><!-- それ以外で表示 -->
		</picture>
		<h1 class="fv-logo" itemscope itemtype="http://schema.org/Organization">
			<a itemprop="url" href="<?php echo home_url(); ?>"><img itemprop="logo" src="<?php echo $uri; ?>/static/assets/images/job_books_logo.svg" /></a>
		</h1>
	</div>
	<nav class="breadcrumb">
		<ol class="d-flex" itemscope itemtype="http://schema.org/BreadcrumbList">
			<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
				<a itemprop="item" href="https://www.music.ac.jp/">
					<span itemprop="name"><i class="fas fa-home"></i>トップ<i class="fas fa-chevron-right arrow"></i></span></a>
				<meta itemprop="position" content="1" />
			</li>
			<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
				<a itemprop="item" href="<?php echo home_url(); ?>">
					<span itemprop="name">お仕事図鑑<i class="fas fa-chevron-right arrow"></i></span></a>
				<meta itemprop="position" content="2" />
			</li>
			<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
				<a itemprop="item" href="<?php echo home_url('/') . $cat_slug; ?>/">
					<span itemprop="name"><?php echo $cat_name; ?>のお仕事<i class="fas fa-chevron-right arrow"></i></span></a>
				<meta itemprop="position" content="3" />
			</li>
			<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
				<div itemprop="item">
					<span itemprop="name"><?php echo get_the_title(); ?></span>
				</div>
				<meta itemprop="position" content="4" />
			</li>
		</ol>
	</nav><!-- .breadcrumb -->
	<article>
		<section class="sec page-head pt-0 pb-0 bg-gray">
			<div class="sec-body">
				<div class="card card-wide mb-0">
					<picture>
						<?php if (has_post_thumbnail()) { //アイキャッチ画像を設定している場合
							the_post_thumbnail('full');
							} else { //アイキャッチ画像を設定していない場合 ?>
							<img src="<?php echo $uri; ?>/static/assets/images/job_<?php echo $cat_num; ?>_mainimg@2x.png" />
						<?php } ?>
					</picture>
					<div class="card-body bg-<?php echo $cat_initial; ?> position-relative">
						<h2 class="card-title color-white mb-0 layer">
							<a href="<?php echo home_url('/') . $cat_slug; ?>/" class="tag mb-2" data-color="white"><?php echo $cat_name; ?>のお仕事</a><br>
							<?php echo get_the_title(); ?>
						</h2>
						<figure class="cat-icon"><img src="<?php echo $uri; ?>/static/assets/images/job_<?php echo $cat_num; ?>_icon_white.svg" /></figure>
					</div>
				</div><!-- .card -->
				<div class="acordion inner layer bg-<?php echo $cat_initial; ?>">
					<h3 class="acordion-ttl">目次</h3>
					<div class="acordion-cont">
						<ul>
							<?php
								$num = 1;
								foreach($fileds as $ket=>$vals):
								if(get_field($vals[0])):
							?>
							<li><a href="#a<?php echo $num; ?>" class="color-white"><?php echo get_the_title() . $vals[2]; ?></a></li>
							<?php
								$num++;
								endif;
								endforeach;
							?>
						</ul>
					</div>
				</div>
			</div><!-- .sec-body -->
		</section><!-- .sec -->
		<section class="sec pt-0 pb-0 bg-gray detail detail-<?php echo $cat_initial; ?>">

			<?php
				$num = 1;
				foreach($fileds as $ket=>$vals):
				if(get_field($vals[0])):
			?>
			<div id="a<?php echo $num; ?>" class="detail-cont">
				<?php if(get_field($vals[1])): ?>
				<figure class="mb-5 inner"><img src="<?php echo get_field($vals[1]); ?>" alt="<?php echo get_the_title() . $vals[2]; ?>"></figure>
				<?php endif; ?>
				<div class="inner">
					<h2 class="detail-ttl" data-color="<?php echo $cat_initial; ?>"><?php echo get_the_title() . $vals[2]; ?></h2>
					<div class="detail-txt">
						<?php echo get_field($vals[0]); ?>
					</div>
				</div>
			</div>
			<?php
				$num++;
				endif;
				endforeach;
			?>

		</section><!-- .sec -->
		<?php
			if(get_category($cat_id)->count > 1) :
		?>
		<section class="sec pt-0 bg-gray">
			<div class="sec-body inner-full">
				<div class="card-wrap row justify-content-center">
					<div class="card card-wide cat-archive col-lg-6">
						<picture>
							<source srcset="<?php echo $uri; ?>/static/assets/images/job_<?php echo $cat_num; ?>_mainimg@2x.png" media="<?php echo $media_pc; ?>"/>
							<img src="<?php echo $uri; ?>/static/assets/images/job_<?php echo $cat_num; ?>_mainimg@2x.png" /><!-- それ以外で表示 -->
						</picture>
						<div class="card-body position-relative">
							<h3 class="card-title layer color-<?php echo $cat_initial; ?>"><?php echo $cat_name; ?><span class="color-black fz-16 font-weight-bold">のお仕事</span></h3>
							<figure class="cat-icon"><img src="<?php echo $uri; ?>/static/assets/images/job_<?php echo $cat_num; ?>_icon.svg" /></figure>
							<div class="btn-list layer">
								<?php
									$args = array(
										'category_name' => $cat_slug,
										'posts_per_page' => -1
									);
									$the_query = new WP_Query( $args );
									if ( $the_query->have_posts() ) :
									while ( $the_query->have_posts() ) : $the_query->the_post();

									if($post_name != get_the_title()):
								?>
								<a href="<?php the_permalink(); ?>" class="btn btn-large btn-full btn-border-left text-left mt-0" data-color="<?php echo $cat_initial; ?>"><?php echo get_the_title(); ?></a>
								<?php
									endif;
									endwhile;
									endif;
									wp_reset_postdata();
								?>
							</div>
							<div class="text-center">
								<a href="<?php echo home_url('/') . $cat_slug; ?>/" class="btn btn-line" data-color="<?php echo $cat_initial; ?>">一覧を見る</a>
							</div>
						</div>
					</div><!-- .card -->
					<?php
						if(have_rows('related-fields')):
					?>
					<div class="card inner cat-archive">
						<div class="card-body position-relative">
							<h3 class="card-title layer color-black p-0">関連分野<span class="color-black fz-16 font-weight-bold">のお仕事</span></h3>
							<div class="btn-list layer">
								<?php
									while(have_rows('related-fields')): the_row();
									$field = get_sub_field('fields');
									$cat = get_the_category($field);

									// 取得した配列から必要な情報を変数に入れる
									$cat_name = $cat[0]->cat_name; // カテゴリー名
									$cat_slug = $cat[0]->category_nicename; // カテゴリースラッグ
									$cat_id = $cat[0]->cat_ID; // カテゴリーID
									$cat_initial = get_field( 'initial', 'category_' . $cat_id );
									$cat_num = get_field( 'num', 'category_' . $cat_id );
								?>
									<a href="<?php echo home_url('/') . $cat_slug . '/' . get_post($field)->post_title; ?>" class="btn btn-large btn-full btn-border-left text-left mt-0" data-color="<?php echo $cat_initial; ?>"><?php echo get_post($field)->post_title; ?></a>
								<?php
									endwhile;
								?>
							</div>
						</div>
					</div><!-- .card -->
					<?php the_sub_field('sub_field_name'); ?>
					<?php
						endif;
					?>
				</div><!-- .card-wrap -->
			</div><!-- .sec-body -->
		</section><!-- .sec -->
		<?php
			endif;
		?>

		<?php get_template_part('populur'); ?>

	</article>

	<div class="back-top">
		<picture>
			<source srcset="<?php echo $uri; ?>/static/assets/images/job_books_mainimg_btnarea_pc@2x.png" media="<?php echo $media_pc; ?>"/>
			<img src="<?php echo $uri; ?>/static/assets/images/job_books_mainimg_btnarea@2x.png" /><!-- それ以外で表示 -->
		</picture>
		<div class="back-top-btnarea">
			<figure class="back-top-logo layer"><img src="<?php echo $uri; ?>/static/assets/images/job_books_logomark.svg" /></figure>
			<a class="btn btn-full btn-line text-center" href="<?php echo home_url(); ?>" data-color="white">TOPページに戻る</a>
		</div>
	</div>

</main><!-- .main -->

<?php get_footer(); ?>
