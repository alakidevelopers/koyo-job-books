var gulp     = require('gulp'),
    concat   = require('gulp-concat'),
    prefixer = require('gulp-autoprefixer'),
    rename   = require('gulp-rename'),
    gutil    = require('gulp-util'),
    sass = require("gulp-sass"),
    sassGlob = require("gulp-sass-glob"),
    uglify = require('gulp-uglify'),
    browserSync = require("browser-sync"),
    connect = require('gulp-connect-php'),
    plumber = require("gulp-plumber"),
    notify  = require('gulp-notify');
    data = require('gulp-data'),
    del = require('del'),
    htmlhint = require('gulp-htmlhint'),
    gulpWatch = require('gulp-watch'),
    imagemin = require('gulp-imagemin'),
    pngquant = require('imagemin-pngquant'),
    mozjpeg = require('imagemin-mozjpeg'),
    changed = require('gulp-changed'),
    docs = '.',
    distDir =  docs + '/assets/images';

//sassファイルのコンパイル
gulp.task('sass', function () {
  return gulp.src('./src/sass/!(_)*.{scss,sass}')
    .pipe(plumber({
      errorHandler: notify.onError("Error: <%= error.message %>")
    }))
    .pipe(sassGlob())
    .pipe(sass({outputStyle: 'compact'}))
    .pipe(prefixer('last 2 version'))
    .pipe(gulp.dest('./assets/css/'))
    .pipe(browserSync.stream());
});

//jsファイルの圧縮
var path = require('path');
var fs = require('fs');
var assetsDir = "./assets/";
var assetsPath = path.resolve(assetsDir);
gulp.task('js', function() {
    gulp.src([path.join(assetsPath, 'js/**/*.js'),path.join('!', assetsPath, 'js/**/*.min.js')])
        .pipe(plumber())
        .pipe(uglify())
        .pipe(rename({extname: '.min.js'}))
        .pipe(gulp.dest(path.join(assetsPath, 'js/')));
});

//browserSync起動
gulp.task('server', function() {
  connect.server({
    port:8001,
    base:'./'
  }, function (){
    browserSync({
      proxy: 'localhost:8001'
    });
  });
});
// gulp.task('server', function() {
//   return browserSync.init({
//     server: {
//       baseDir: './'
//     }
//   })
// });

// htmllint
gulp.task('html-hint',function(){
    gulp.src(["./cn/*.html","./jp/*.html"])
        .pipe(plumber({
            errorHandler: notify.onError('Error: <%= error.message %>')
        }))
        .pipe(htmlhint('./src/htmlhint/htmlhintrc.json'))
        .pipe(htmlhint.failReporter());
});

gulp.task('watch',["server"], function () {
    gulp.watch('./src/js/**/*.js', ['webpack']),
    gulp.watch('./src/sass/**/*.{scss,sass}', ['sass']);
    gulp.watch('./**/*.html').on('change', function() {
      browserSync.reload()
    });
    gulp.watch('./**/*.php').on('change', function() {
      browserSync.reload()
    });
    gulp.watch('./assets/**/*.js').on('change', function() {
      browserSync.reload()
    });
    gulp.watch([path.join(assetsPath, 'js/**/*.js'),path.join('!', assetsPath, 'js/**/*.min.js')],['js']);
});

gulp.task("default",["watch"]);
