<?php
	$page = "/"; /* ディレクトリルートからのパス */
	$cat = "top"; /* 基本的には、ファイル名orフォルダ名を記載（トップページは"top"で） */
	$ttl = "xxxxx"; /* titleタグに入れる文言 */
	$desc = "xxxxx"; /* discriptionタグに入れる文言 */
	include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/common.php');
	include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header.php');
?>
<main class="main cat-top page">
	<div class="fv">
		<picture>
			<source srcset="/assets/images/job_books_mainimg_page@2x.png" media="<?php echo $media_pc; ?>"/>
			<img src="/assets/images/job_books_mainimg_page@2x.png" /><!-- それ以外で表示 -->
		</picture>
		<h1 class="fv-logo" itemscope itemtype="http://schema.org/Organization">
			<a itemprop="url" href="/"><img itemprop="logo" src="/assets/images/job_books_logo.svg" /></a>
		</h1>
	</div>
	<nav class="breadcrumb">
		<ol class="d-flex" itemscope itemtype="http://schema.org/BreadcrumbList">
			<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
				<a itemprop="item" href="https://example.com/arts">
					<span itemprop="name"><i class="fas fa-home"></i>トップ<i class="fas fa-chevron-right arrow"></i></span></a>
				<meta itemprop="position" content="1" />
			</li>
			<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
				<a itemprop="item" href="https://example.com/arts">
					<span itemprop="name">お仕事図鑑<i class="fas fa-chevron-right arrow"></i></span></a>
				<meta itemprop="position" content="2" />
			</li>
			<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
				<div itemprop="item" href="https://example.com/arts/books">
					<span itemprop="name">ヴォーカリスト・ミュージシャンのお仕事</span>
				</div>
				<meta itemprop="position" content="3" />
			</li>
		</ol>
	</nav><!-- .breadcrumb -->
	<article>
		<section class="sec page-head pt-0 pb-3 bg-gray">
			<div class="sec-body">
				<div class="card card-wide">
					<picture>
						<source srcset="/assets/images/job_01_mainimg@2x.png" media="<?php echo $media_pc; ?>"/>
						<img src="/assets/images/job_01_mainimg@2x.png" /><!-- それ以外で表示 -->
					</picture>
					<div class="card-body bg-vm position-relative">
						<h2 class="card-title color-white mb-0 layer">ヴォーカリスト・ミュージシャン<span class="fz-16 font-weight-bold">のお仕事一覧</span></h2>
						<figure class="cat-icon"><img src="/assets/images/job_01_icon_white.svg" /></figure>
					</div>
				</div><!-- .card -->
			</div><!-- .sec-body -->
		</section><!-- .sec -->
		<section class="sec cat-top-list pt-0 bg-gray">
			<div class="inner">
				<div class="card-wrap">
					<div class="card">
						<picture>
							<img src="/assets/images/dammy.png" />
						</picture>
						<div class="card-body">
							<h3 class="card-title">テキストテキストテキストテキストテキストテキスト</h3>
							<div class="card-text sentence">
								<p>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</p>
							</div>
							<a href="#" class="btn mt-5" data-color="vm">ヴォーカリストのお仕事を見る</a>
						</div>
					</div><!-- .card -->
					<div class="card">
						<picture>
							<img src="/assets/images/dammy.png" />
						</picture>
						<div class="card-body">
							<h3 class="card-title">テキストテキストテキストテキストテキストテキスト</h5>
							<div class="card-text sentence">
								<p>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</p>
							</div>
							<a href="#" class="btn mt-5" data-color="vm">ヴォーカリストのお仕事を見る</a>
						</div>
					</div><!-- .card -->
					<div class="card">
						<picture>
							<img src="/assets/images/dammy.png" />
						</picture>
						<div class="card-body">
							<h3 class="card-title">テキストテキストテキストテキストテキストテキスト</h5>
							<div class="card-text sentence">
								<p>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</p>
							</div>
							<a href="#" class="btn mt-5" data-color="vm">ヴォーカリストのお仕事を見る</a>
						</div>
					</div><!-- .card -->
				</div><!-- .card-wrap -->
			</div><!-- .inner -->
		</section><!-- .sec -->
		<section class="sec pt-0 pb-0 bg-gray other-cat">
			<h2 class="fz-20 font-weight-bold mb-5 inner">その他の分野<span class="fz-16 font-weight-bold">のお仕事</span></h2>
			<ul class="cat-other-list">
				<li data-color="esm">
					<a href="#">
						<figure class="cat-other-icon"><img src="/assets/images/job_02_icon_color.svg" alt=""></figure>
						<div class="cat-other-ttl">
							e-sports・スマホアプリ・<br>
							映像<span>のお仕事</span>
						</div>
					</a>
				</li>
				<li data-color="esm">
					<a href="#">
						<figure class="cat-other-icon"><img src="/assets/images/job_02_icon_color.svg" alt=""></figure>
						<div class="cat-other-ttl">
							e-sports・スマホアプリ・<br>
							映像<span>のお仕事</span>
						</div>
					</a>
				</li>
				<li data-color="esm">
					<a href="#">
						<figure class="cat-other-icon"><img src="/assets/images/job_02_icon_color.svg" alt=""></figure>
						<div class="cat-other-ttl">
							e-sports・スマホアプリ・<br>
							映像<span>のお仕事</span>
						</div>
					</a>
				</li>
			</ul>
		</section><!-- .sec -->
		<section class="sec other-archive">
			<div class="inner">
				<div class="sec-ttlarea color-white layer">
					<div class="sec-enttl">Popular jobs</div>
					<h2 class="sec-ttl">高校生が<br>よく見ているお仕事</h2>
				</div><!-- .sec-ttlarea -->
				<div class="card-wrap">
					<div class="card">
						<div class="card-badge"><img src="/assets/images/rank1.svg" alt="1"></div>
						<picture>
							<img src="/assets/images/dammy.png" />
						</picture>
						<div class="card-body">
							<div class="tag-cloud mb-3">
								<div class="tag" data-color="vm">ヴォーカリスト・ミュージシャンのお仕事</div>
							</div>
							<h3 class="card-title">テキストテキストテキストテキストテキストテキスト</h5>
							<div class="card-text sentence">
								<p>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</p>
							</div>
							<a href="#" class="btn mt-5" data-color="vm">ヴォーカリストのお仕事を見る</a>
						</div>
					</div><!-- .card -->
					<div class="card">
						<div class="card-badge"><img src="/assets/images/rank2.svg" alt="2"></div>
						<picture>
							<img src="/assets/images/dammy.png" />
						</picture>
						<div class="card-body">
							<div class="tag-cloud mb-3">
								<div class="tag" data-color="vm">ヴォーカリスト・ミュージシャンのお仕事</div>
							</div>
							<h3 class="card-title">テキストテキストテキストテキストテキストテキスト</h5>
							<div class="card-text sentence">
								<p>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</p>
							</div>
							<a href="#" class="btn mt-5" data-color="vm">ヴォーカリストのお仕事を見る</a>
						</div>
					</div><!-- .card -->
					<div class="card">
						<div class="card-badge"><img src="/assets/images/rank3.svg" alt="3"></div>
						<picture>
							<img src="/assets/images/dammy.png" />
						</picture>
						<div class="card-body">
							<div class="tag-cloud mb-3">
								<div class="tag" data-color="vm">ヴォーカリスト・ミュージシャンのお仕事</div>
							</div>
							<h3 class="card-title">テキストテキストテキストテキストテキストテキスト</h5>
							<div class="card-text sentence">
								<p>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</p>
							</div>
							<a href="#" class="btn mt-5" data-color="vm">ヴォーカリストのお仕事を見る</a>
						</div>
					</div><!-- .card -->
				</div><!-- .card-wrap -->
			</div><!-- .inner -->
		</section><!-- .sec -->
	</article>

	<div class="back-top">
		<picture>
			<source srcset="/assets/images/job_books_mainimg_btnarea@2x.png" media="<?php echo $media_pc; ?>"/>
			<img src="/assets/images/job_books_mainimg_btnarea@2x.png" /><!-- それ以外で表示 -->
		</picture>
		<div class="back-top-btnarea">
			<figure class="back-top-logo layer"><img src="/assets/images/job_books_logomark.svg" /></figure>
			<a class="btn btn-full btn-line" href="/" data-color="white">TOPページに戻る</a>
		</div>
	</div>

</main><!-- .main -->
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>