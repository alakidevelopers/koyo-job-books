<a href="#wrap" class="pagetop"><i class="fa fa-angle-up" aria-hidden="true"></i></a>

<section id="footArea">
	<div id="spc">
	<h3 class="center"><i class="fas fa-plus-circle"></i> 訪問者別メニューを表示する <i class="fas fa-plus-circle"></i></h3>
	<ul class="requirement-nav_list">
	<li><a href="https://www.music.ac.jp/visitor/applicant.html">入学希望の方へ</a></li>
	<li><a href="https://www.music.ac.jp/visitor/student.html">在校生の方へ</a></li>
	<li><a href="https://www.music.ac.jp/visitor/alumni.html">卒業生の方へ</a></li>
	<li><a href="https://www.music.ac.jp/visitor/parents.html">保護者の方へ</a></li>
	<li><a href="https://www.music.ac.jp/visitor/abroad.html">留学生の方へ</a></li>
	<li><a href="https://www.music.ac.jp/visitor/company.html">企業の方へ</a></li>
	</ul>
	</div>
</section>

<footer>
	<div class="column">

	<div class="flex3">
	<h2><a href="https://www.music.ac.jp/about">koyoについて <i class="fas fa-angle-down"></i></a></h2>
	<ul class="maBottom10">
	<li><a href="https://www.music.ac.jp/about/internationaleducation.html">世界基準を学ぶ</a></li>
	<li><a href="https://www.music.ac.jp/about/berklee.html">バークリー音楽大学 提携校</a></li>
	<li><a href="https://www.music.ac.jp/about/access.html">アクセス</a></li>
	<li><a href="https://www.music.ac.jp/about/facility.html" target="_blank">フロアガイド <i class="far fa-window-restore"></i></a></li>
	</ul>

	<h2><a href="https://www.music.ac.jp/jazz">JAZZ <i class="fas fa-angle-down"></i></a></h2>
	<ul>
	<li><a href="https://www.music.ac.jp/jazz/graduate.html">卒業生インタビュー</a></li>
	<li><a href="https://www.music.ac.jp/jazz/kobe.html">神戸でジャズを学ぶ</a></li>
	<li><a href="https://www.music.ac.jp/jazz/instructors.html">ジャズ講師紹介</a></li>
	</ul>

	</div>


	<div class="flex3">
	<h2><a href="https://www.music.ac.jp/course/">コース <i class="fas fa-angle-down"></i></a></h2>

	<p>MUSIC <i class="fas fa-angle-down"></i></p>
	<ul>
	<li><a href="https://www.music.ac.jp/course/music/musician.html">海外音楽留学</a></li>
	<li><a href="https://www.music.ac.jp/course/music/jazz.html">JAZZミュージシャン</a></li>
	<li><a href="https://www.music.ac.jp/course/music/vocal.html">ヴォーカル</a></li>
	<li><a href="https://www.music.ac.jp/course/music/professional.html">ミュージシャン</a></li>
	<li><a href="https://www.music.ac.jp/course/music/business.html">コンサートスタッフ</a></li>
	<li><a href="https://www.music.ac.jp/course/music/recording.html">DJ・作曲・DTM</a></li>
	<li><a href="https://www.music.ac.jp/course/music/e-sports.html">e-sports</a></li>
	<li><a href="https://www.music.ac.jp/course/music/application.html">スマホアプリクリエイター</a></li>
	</ul>



	<p class="pdTop2">DANCE<i class="fas fa-angle-down"></i></p>
	<ul>
	<li><a href="https://www.music.ac.jp/course/dance/abroad.html">海外ダンス留学</a></li>
	<li><a href="https://www.music.ac.jp/course/dance/dance.html">ダンス</a></li>
	<li><a href="https://www.music.ac.jp/course/dance/actor.html">俳優・声優</a></li>
	</ul>



	<p><a href="https://www.music.ac.jp/course/graduates.html">卒業生インタビュー</a></p>
	<p><a href="https://www.music.ac.jp/course/instructors.html">講師紹介</a></p>


	</div>


	<div class="flex2">
	<h2><a href="https://www.music.ac.jp/debut/">デビュー・就職 <i class="fas fa-angle-down"></i></a></h2>

	<ul>

	<li><a href="https://www.music.ac.jp/debut/debutsystem.html">デビューシステム</a></li>
	<li><a href="https://www.music.ac.jp/debut/educationsystem.html">企業プロジェクト</a></li>
	<li><a href="https://www.music.ac.jp/debut/employmentsystem.html">就職システム</a></li>
	</ul>

	</div>



	<div class="flex2">
	<h2><a href="https://www.music.ac.jp/oversea/">海外で学ぶ <i class="fas fa-angle-down"></i></a></h2>
	<ul>

	<li><a href="https://www.music.ac.jp/oversea/graduate.html">海外で学ぶ卒業生</a></li>
	<li><a href="https://www.music.ac.jp/oversea/studyabroad.html">海外留学の流れ</a></li>
	<li><a href="https://www.music.ac.jp/oversea/support-music.html">留学サポート［音楽］</a></li>
	<li><a href="https://www.music.ac.jp/oversea/support-dance.html">留学サポート［ダンス］</a></li>
	</ul>
	</div>




	<div class="flex2">
	<h2><a href="https://www.music.ac.jp/schoollife/">学園生活 <i class="fas fa-angle-down"></i></a></h2>
	<ul>
	<li><a href="https://www.music.ac.jp/schoollife/dormitory.html">学生寮・ひとり暮らし</a></li>
	<li><a href="https://www.music.ac.jp/about/facility.html">フロアガイド <i class="far fa-window-restore"></i></a></li>
	</ul>
	</div>


	<div class="flex2">
	<h2><a href="https://www.music.ac.jp/application_requirements/">募集要項 <i class="fas fa-angle-down"></i></a></h2>
	<ul>
	<li><a href="https://www.music.ac.jp/application_requirements/eligibility.html">入学資格・出願方法</a></li>
	<li><a href="https://www.music.ac.jp/application_requirements/ao.html">AO入学について</a></li>
	<li><a href="https://www.music.ac.jp/application_requirements/aoapplication.html">ネット出願AO入学</a></li>
	<li><a href="https://www.music.ac.jp/application_requirements/society.html">社会人入学について</a></li>
	<li><a href="https://www.music.ac.jp/application_requirements/recommended.html">推薦入学について</a></li>
	<li><a href="https://www.music.ac.jp/application_requirements/general.html">一般入学について</a></li>
	<li><a href="https://www.music.ac.jp/application_requirements/flow.html">出願から合格まで</a></li>
	<li><a href="https://www.music.ac.jp/application_requirements/pdf/howto.pdf" target="_blank">出願書類記入方法<i class="far fa-file-pdf"></i></a></li>
	<li><a href="https://www.music.ac.jp/application_requirements/scholarship.html">学費サポート</a></li>

	</ul>
	</div>

	</div>


	<p class="center pdTop6 maBottom2 logo"><a href="https://www.music.ac.jp"><img class="d-inline" src="https://www.music.ac.jp/asset/img/f_logo.png"></a>　<a href="https://www.music.ac.jp/highschool/"><img class="d-inline" src="https://www.music.ac.jp/asset/img/f_logo_hs.png"></a></p>
	<p class="center pdTop2 pdBottom2"><a href="https://www.music.ac.jp/public_info/">学校情報公開</a>  ／ <a href="https://www.music.ac.jp/kyufu.html">高等教育修学支援制度</a>  ／ <a href="https://www.music.ac.jp/socialpolicy.html">ソーシャルメディアポリシー</a>  ／ <a href="https://www.music.ac.jp/privacy.html">個人情報ポリシー</a>　<br class="sp"><span class="txtS200"><a href="http://line.naver.jp/ti/p/%40ady1027z" target="_blank"><i class="fab fa-line"></i></a> <a href="https://www.instagram.com/kobe_koyo/" target="_blank"><i class="fab fa-instagram"></i></a> <a href="https://twitter.com/kobe_ksm" target="_blank"><i class="fab fa-twitter-square"></i></a> <a href="https://www.youtube.com/channel/UCvsu6DqzhnHkFxXpbekhuzA" target="_blank"><i class="fab fa-youtube-square"></i></a></span></p>


	<p class="center">神戸・甲陽音楽&amp;ダンス専門学校  <br class="sp">0120-117-540</p>
	<p class="center">© KOYO SCHOOL OF MUSIC &amp; DANCE</p>

	<nav id="navFixedFoot">
	<ul>
		<li><a href="https://www.music.ac.jp/event/form.html?form_code=1298"><i class="fas fa-book-open"></i> 資料請求</a></li>
		<li><a href="https://www.music.ac.jp/event/"><i class="fas fa-calendar-alt"></i> 体験入学</a></li>
		<li><a href="https://www.music.ac.jp/about/access.html"><i class="fas fa-map-marker-alt"></i> アクセス</a></li>
		<li><a href="tel:0120-117-540"><i class="fas fa-phone"></i> TEL</a></li>
	</ul>
	</nav>
</footer>

</div><!-- #wrap -->

<script src="https://code.jquery.com/jquery-3.1.0.min.js"></script>
<script src="/assets/js/popper.min.js"></script>
<script src="/assets/js/bootstrap.min.js"></script>
<script src="/assets/js/jquery.inview.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/gsap/latest/TweenMax.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.0.1/TimelineMax.min.js"></script>
<script src="/assets/js/common.js"></script>
<script src="/assets/js/jquery.fittext.min.js"></script>
<script src="/assets/js/jquery.lettering.min.js"></script>
<script src="/assets/js/jquery.textillate.min.js"></script>
<script src="/assets/js/skrollr.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/layzr.js/1.4.3/layzr.min.js"></script>
<script>
</script>
</body>
</html>
