<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="<?php echo $desc; ?>全ページ共通のdescriptionを記入">
<meta name="thumbnail" content="">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title><?php echo $ttl; ?> | 全ページ共通のtitleを記入</title>
<link href="https://fonts.googleapis.com/css2?family=Noto+Sans+JP:wght@300;500;700;900&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css2?family=Hind:wght@300;500;700&display=swap" rel="stylesheet">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
<link rel="stylesheet" href="/assets/css/bootstrap.min.css">
<link rel="stylesheet" href="https://www.music.ac.jp/asset/css/base.css?v=38">
<link rel="stylesheet" href="/assets/css/style.css">
<link rel="stylesheet" href="/assets/css/animate.css">
<link rel="alternate" href="/" hreflang="ja">
<link rel="canonical" href="/">