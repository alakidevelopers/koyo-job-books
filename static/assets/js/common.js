$(function(){

    var userAgent = window.navigator.userAgent.toLowerCase();
    if( userAgent.indexOf('msie') != -1 || userAgent.indexOf('trident') != -1 ){
        var script = document.createElement('script');
        script.src = '/assets/js/picturefill.js';
        document.body.appendChild(script);
    }

    var layzr = new Layzr();

    /* event tracking */
    /*
        クリックを計測したい要素に、下記の属性を指定してください
        data-push="true" data-cat="..."
    */
    $('[data-push="true"]').on('click',function(){
        var cat = $(this).attr('data-cat');
        var path = location.href;
        dataLayer.push({
            event: 'bridal',
            eventCategory: cat,
            eventAction: 'クリック',
            eventLabel: path
        });
    });

    /* scroll */
    $('a[href^="#"]').click(function() {
        var speed = 500;
        var href= $(this).attr("href");
        var target = $(href == "#" || href == "" ? 'html' : href);
        var position = target.offset().top;
        $('body,html').animate({scrollTop:position}, speed, 'swing');
        return false;
    });

    /* inview */
    $('.inview-js').each(function(){
        $(this).on('inview', function(event, isInView, visiblePartX, visiblePartY) {
            if (isInView) {
                $(this).addClass('inview-js-on');
            }
        });
    });

    $('.txt-animate').each(function(){
        $(this).on('inview', function(event, isInView, visiblePartX, visiblePartY) {
            if (isInView) {
                $(this).addClass('txt-animate-on');
                $(this).textillate({
                    initialDelay: 400,
                    in: { effect: 'fadeInUp' }
                });
            }
        });
    });

    /* 外部ページからのページ内リンク 位置調整 */
    $(window).on('load', function() {
        var headerHeight = $('.header').height();
        var url = $(location).attr('href');
        if(url.indexOf("?id=") != -1){
            var id = url.split("?id=");
            var $target = $('#' + id[id.length - 1]);
            if($target.length){
                var pos = $target.offset().top-headerHeight;
                $("html, body").animate({scrollTop:pos}, 400);
            }
        }
    });

    /* linkarea */
    $('.link-area').on('click',function(){
        window.location = $(this).find('.link-area-target').attr('href');
    });

    /* ファーストビュースクロール後の処理 */
    $(window).on('load scroll',function(){
        var st = $(window).scrollTop();
        if(st > 200) {
            $('body').addClass('body-scrolled');
        } else {
            $('body').removeClass('body-scrolled');
        }
    });

    /* スマホのみタップで電話可能にする */
    var ua = navigator.userAgent;
    if(ua.indexOf('iPhone') > 0 || ua.indexOf('Android') > 0){
        $('.tel-link').each(function(){
            var str = $(this).text();
            $(this).html($('<a>').attr('href', 'tel:' + str.replace(/-/g, '')).append(str + '</a>'));
        });
    }

    /* modal */
    var mtrigger = $('.modal-trigger');
    var mwrap = $('.modal-wrap');
    var mcont = $('.modal-cont');
    var mclose = $('.modal-close');
    mtrigger.on('click',function(){
        var num = $(this).attr('data-modal');
        modalOpen(num-1);
    });
    mclose.on('click',function(){
        modalClose();
    });


    function modalOpen(i){
        mwrap.addClass('modal-wrap-on');
        mcont.eq(i).fadeIn(500);
        overlay.addClass('overlay-on');
        state = true;
    }
    function modalClose(){
        mwrap.removeClass('modal-wrap-on');
        mcont.fadeOut(500);
        overlay.removeClass('overlay-on');
        state = false;
    }

    /* menu */
    // var trigger = $('.menu-trigger'),
    // menu = $('.menu'),
    // overlay = $('.overlay'),
    // state = false,
    // scrollpos;
    // trigger.on('click',function(){
    //     if(state == false) {
    //         menuOpen();
    //     } else {
    //         menuClose();
    //     }
    // });
    // $(document).click(function(event) {
    //     if($(event.target).closest('.overlay-on').length) {
    //         if(menu.hasClass('menu-on')) {
    //             menuClose();
    //         }
    //         if(mwrap.hasClass('modal-wrap-on')) {
    //             modalClose();
    //         }
    //     }
    // });
    // $('.menu a[href^="#"]').on('click',function(){
    //     menuClose();
    // });
    // function menuOpen(){
    //     trigger.addClass('menu-trigger-on');
    //     menu.addClass('menu-on');
    //     overlay.addClass('overlay-on');
    //     scrollpos = $(window).scrollTop();
    //     $('body').addClass('body-fixed').css({'top': -scrollpos});
    //     state = true;
    // }
    // function menuClose(){
    //     trigger.removeClass('menu-trigger-on');
    //     menu.removeClass('menu-on');
    //     overlay.removeClass('overlay-on');
    //     $('body').removeClass('body-fixed').css({'top': 0});
    //     window.scrollTo( 0 , scrollpos );
    //     state = false;
    // }

    var windowWidth = $(window).width();
    var windowSm = 780;


    $('#show1,#wshow1').hover(function(){
        $('#wshow2,#wshow3,#wshow4,#wshow5,#wshow6,#wshow7').hide();
        $('#wshow1').stop().slideToggle();
    
    });
    $('#show2,#wshow2').hover(function(){
        $('#wshow1,#wshow3,#wshow4,#wshow5,#wshow6,#wshow7').hide();
        $('#wshow2').stop().slideToggle();
    });
    $('#show3,#wshow3').hover(function(){
        $('#wshow1,#wshow2,#wshow4,#wshow5,#wshow6,#wshow7').hide();
        $('#wshow3').stop().slideToggle();
    });
    $('#show4,#wshow4').hover(function(){
        $('#wshow1,#wshow2,#wshow3,#wshow5,#wshow6,#wshow7').hide();
        $('#wshow4').stop().slideToggle();
    });
    $('#show5,#wshow5').hover(function(){
        $('#wshow1,#wshow2,#wshow3,#wshow4,#wshow6,#wshow7').hide();
        $('#wshow5').stop().slideToggle();
    });
    $('#show6,#wshow6').hover(function(){
        $('#wshow1,#wshow2,#wshow3,#wshow4,#wshow5,#wshow7').hide();
        $('#wshow6').stop().slideToggle();
    });
    $('#show7,#wshow7').hover(function(){
        $('#wshow1,#wshow2,#wshow3,#wshow4,#wshow5,#wshow6').hide();
        $('#wshow7').stop().slideToggle();
    });

    $('#spc h3').on('click',function(){
        $('#spc ul').slideToggle();
        var c = $('#spc h3').html();
        if(c=='<i class="fas fa-plus-circle"></i> 訪問者別メニューを表示する <i class="fas fa-plus-circle"></i>'){
            $('#spc h3').html('<i class="fas fa-minus-circle"></i> 訪問者別メニューを表示する <i class="fas fa-minus-circle"></i>');
        }else{
            $('#spc h3').html('<i class="fas fa-plus-circle"></i> 訪問者別メニューを表示する <i class="fas fa-plus-circle"></i>');
        }
    });

    $('#spMenu > div > h3').on('click',function(){
        var icon = $(this).children('i').attr('class');
        if(icon=='fas fa-plus'){
            $(this).children('i').removeClass().addClass('fas fa-minus');
        }else{
            $(this).children('i').removeClass().addClass('fas fa-plus');
        }
        $(this).next('.drawer').slideToggle();
    });

    var spmenu = $("#spMenu");
    var tl_1 = new TimelineMax();
    tl_1.set(spmenu,{autoAlpha: 0,scale: 1});
        tl_1.to('#mTrigger', 0.1, {zIndex:1000});
        tl_1.to(spmenu, 1, {display:'block',autoAlpha:1,scale:1},'-=0.1');
        tl_1.to('#mTrigger p:nth-child(1)', 0.7, {x:10,y:17,rotation:'50',backgroundColor:'#FFFFFF', width:"20px",display:'block',autoAlpha:1,scale:1.2},'-=0.7');
        tl_1.to('#mTrigger p:nth-child(3)', 0.7, {x:9.5,y:-3,rotation:'-50',backgroundColor:'#FFFFFF', width:"20px",display:'block',autoAlpha:1,scale:1.2,zIndex:1000},'-=0.7');
        tl_1.to('#mTrigger p:nth-child(2),#mTrigger p:nth-child(4)', 0.7, {autoAlpha:0,scale:1.2},'-=0.7');
        tl_1.pause();

    $('#mTrigger').on('click',function(){
        if($('#mTrigger').hasClass('on')){
            tl_1.reverse();
            $('#mTrigger').removeClass("on");		
        }else{
            tl_1.play();
            $('#mTrigger').addClass("on");
        }

    });

    $('.acordion-ttl').on('click',function(){
        $(this).toggleClass('on');
        $(this).next().slideToggle();
    });
});
