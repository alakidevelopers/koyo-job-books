<?php
	$page = "/"; /* ディレクトリルートからのパス */
	$cat = "top"; /* 基本的には、ファイル名orフォルダ名を記載（トップページは"top"で） */
	$ttl = "xxxxx"; /* titleタグに入れる文言 */
	$desc = "xxxxx"; /* discriptionタグに入れる文言 */
	include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/common.php');
	include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header.php');
?>
<main class="main front">
	<div class="fv">
		<picture>
			<source srcset="/assets/images/job_books_mainimg_pc@2x.png" media="<?php echo $media_pc; ?>"/>
			<img src="/assets/images/job_books_mainimg@2x.png" /><!-- それ以外で表示 -->
		</picture>
		<h1 class="fv-logo" itemscope itemtype="http://schema.org/Organization">
			<a itemprop="url" href="/"><img itemprop="logo" src="/assets/images/job_books_logo.svg" /></a>
		</h1>
		<nav class="breadcrumb">
			<ol class="d-flex" itemscope itemtype="http://schema.org/BreadcrumbList">
				<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
					<a itemprop="item" href="https://example.com/arts">
						<span itemprop="name"><i class="fas fa-home"></i>トップ<i class="fas fa-chevron-right arrow"></i></span></a>
					<meta itemprop="position" content="1" />
				</li>
				<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
					<div itemprop="item" href="https://example.com/arts/books">
						<span itemprop="name">お仕事図鑑</span>
					</div>
					<meta itemprop="position" content="2" />
				</li>
			</ol>
		</nav><!-- .breadcrumb -->
	</div>
	<div class="intro pt-0 shadow-btm pb-4">
		<div class="sec-ttlarea color-white layer">
			<h2 class="sec-ttl">神戸・甲陽で目指せる仕事</h2>
			<p class="font-weight-bold">世の中にはたくさんの仕事があります。<br>
			キミの「好き」を「仕事」にできる<br class="d-md-none">
			職業を見つけよう！</p>
		</div><!-- .sec-ttlarea -->
	</div>
	<article>
		<section class="sec pt-0 bg-gray cat-archive">
			<div class="inner-full">
				<div class="sec-body row">
					<div class="card-wrap col-md-6">
						<div class="card card-wide">
							<picture>
								<source srcset="/assets/images/job_01_mainimg@2x.png" media="<?php echo $media_pc; ?>"/>
								<img src="/assets/images/job_01_mainimg@2x.png" /><!-- それ以外で表示 -->
							</picture>
							<div class="card-body position-relative">
								<h3 class="card-title layer color-vm">ヴォーカリスト・<br>ミュージシャン<span class="color-black fz-16 font-weight-bold">のお仕事</span></h5>
								<figure class="cat-icon"><img src="/assets/images/job_01_icon.svg" /></figure>
								<div class="btn-list layer">
									<a href="#" class="btn btn-large btn-full btn-border-left text-left mt-0" data-color="vm">ヴォーカリスト</a>
									<a href="#" class="btn btn-large btn-full btn-border-left text-left mt-0" data-color="vm">ヴォーカリスト</a>
								</div>
								<div class="text-center">
									<a href="#" class="btn btn-line" data-color="vm">一覧を見る</a>
								</div>
							</div>
						</div><!-- .card -->
						<div class="card card-wide">
							<picture>
								<source srcset="/assets/images/job_01_mainimg@2x.png" media="<?php echo $media_pc; ?>"/>
								<img src="/assets/images/job_01_mainimg@2x.png" /><!-- それ以外で表示 -->
							</picture>
							<div class="card-body position-relative">
								<h3 class="card-title layer color-vm">ヴォーカリスト・<br>ミュージシャン<span class="color-black fz-16 font-weight-bold">のお仕事</span></h5>
								<figure class="cat-icon"><img src="/assets/images/job_01_icon.svg" /></figure>
								<div class="btn-list layer">
									<a href="#" class="btn btn-large btn-full btn-border-left text-left mt-0" data-color="vm">ヴォーカリスト</a>
									<a href="#" class="btn btn-large btn-full btn-border-left text-left mt-0" data-color="vm">ヴォーカリスト</a>
									<a href="#" class="btn btn-large btn-full btn-border-left text-left mt-0" data-color="vm">ヴォーカリスト</a>
									<a href="#" class="btn btn-large btn-full btn-border-left text-left mt-0" data-color="vm">ヴォーカリスト</a>
								</div>
								<div class="text-center">
									<a href="#" class="btn btn-line" data-color="vm">一覧を見る</a>
								</div>
							</div>
						</div><!-- .card -->
					</div><!-- .card-wrap -->
					<div class="card-wrap col-md-6">
						<div class="card card-wide">
							<picture>
								<source srcset="/assets/images/job_01_mainimg@2x.png" media="<?php echo $media_pc; ?>"/>
								<img src="/assets/images/job_01_mainimg@2x.png" /><!-- それ以外で表示 -->
							</picture>
							<div class="card-body position-relative">
								<h3 class="card-title layer color-vm">ヴォーカリスト・<br>ミュージシャン<span class="color-black fz-16 font-weight-bold">のお仕事</span></h5>
								<figure class="cat-icon"><img src="/assets/images/job_01_icon.svg" /></figure>
								<div class="btn-list layer">
									<a href="#" class="btn btn-large btn-full btn-border-left text-left mt-0" data-color="vm">ヴォーカリスト</a>
									<a href="#" class="btn btn-large btn-full btn-border-left text-left mt-0" data-color="vm">ヴォーカリスト</a>
									<a href="#" class="btn btn-large btn-full btn-border-left text-left mt-0" data-color="vm">ヴォーカリスト</a>
									<a href="#" class="btn btn-large btn-full btn-border-left text-left mt-0" data-color="vm">ヴォーカリスト</a>
								</div>
								<div class="text-center">
									<a href="#" class="btn btn-line" data-color="vm">一覧を見る</a>
								</div>
							</div>
						</div><!-- .card -->
						<div class="card card-wide">
							<picture>
								<source srcset="/assets/images/job_01_mainimg@2x.png" media="<?php echo $media_pc; ?>"/>
								<img src="/assets/images/job_01_mainimg@2x.png" /><!-- それ以外で表示 -->
							</picture>
							<div class="card-body position-relative">
								<h3 class="card-title layer color-vm">ヴォーカリスト・<br>ミュージシャン<span class="color-black fz-16 font-weight-bold">のお仕事</span></h5>
								<figure class="cat-icon"><img src="/assets/images/job_01_icon.svg" /></figure>
								<div class="btn-list layer">
									<a href="#" class="btn btn-large btn-full btn-border-left text-left mt-0" data-color="vm">ヴォーカリスト</a>
									<a href="#" class="btn btn-large btn-full btn-border-left text-left mt-0" data-color="vm">ヴォーカリスト</a>
									<a href="#" class="btn btn-large btn-full btn-border-left text-left mt-0" data-color="vm">ヴォーカリスト</a>
									<a href="#" class="btn btn-large btn-full btn-border-left text-left mt-0" data-color="vm">ヴォーカリスト</a>
								</div>
								<div class="text-center">
									<a href="#" class="btn btn-line" data-color="vm">一覧を見る</a>
								</div>
							</div>
						</div><!-- .card -->
					</div><!-- .card-wrap -->
				</div><!-- .sec-body -->
			</div>
		</section><!-- .sec -->
		<section class="sec other-archive">
			<div class="inner">
				<div class="sec-ttlarea color-white layer">
					<div class="sec-enttl">Popular jobs</div>
					<h2 class="sec-ttl">高校生が<br class="d-md-none">よく見ているお仕事</h2>
				</div><!-- .sec-ttlarea -->
				<div class="card-wrap row">
					<div class="card col-md-4 link-area">
						<div class="card-badge"><img src="/assets/images/rank1.svg" alt="1"></div>
						<picture>
							<img src="/assets/images/dammy@2x.png" />
						</picture>
						<div class="card-body">
							<div class="tag-cloud mb-3">
								<div class="tag" data-color="vm">ヴォーカリスト・ミュージシャンのお仕事</div>
							</div>
							<h3 class="card-title">テキストテキストテキストテキストテキストテキスト</h5>
							<div class="card-text sentence">
								<p>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</p>
							</div>
							<a href="#" class="btn mt-5 link-area-target" data-color="vm">ヴォーカリストのお仕事を見る</a>
						</div>
					</div><!-- .card -->
					<div class="card col-md-4 link-area">
						<div class="card-badge"><img src="/assets/images/rank2.svg" alt="2"></div>
						<picture>
							<img src="/assets/images/dammy@2x.png" />
						</picture>
						<div class="card-body">
							<div class="tag-cloud mb-3">
								<div class="tag" data-color="vm">ヴォーカリスト・ミュージシャンのお仕事</div>
							</div>
							<h3 class="card-title">テキストテキストテキストテキストテキストテキスト</h5>
							<div class="card-text sentence">
								<p>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</p>
							</div>
							<a href="#" class="btn mt-5 link-area-target" data-color="vm">ヴォーカリストのお仕事を見る</a>
						</div>
					</div><!-- .card -->
					<div class="card col-md-4 link-area">
						<div class="card-badge"><img src="/assets/images/rank3.svg" alt="3"></div>
						<picture>
							<img src="/assets/images/dammy@2x.png" />
						</picture>
						<div class="card-body">
							<div class="tag-cloud mb-3">
								<div class="tag" data-color="vm">ヴォーカリスト・ミュージシャンのお仕事</div>
							</div>
							<h3 class="card-title">テキストテキストテキストテキストテキストテキスト</h5>
							<div class="card-text sentence">
								<p>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</p>
							</div>
							<a href="#" class="btn mt-5 link-area-target" data-color="vm">ヴォーカリストのお仕事を見る</a>
						</div>
					</div><!-- .card -->
				</div><!-- .card-wrap -->
			</div><!-- .inner -->
		</section><!-- .sec -->
	</article>

</main><!-- .main -->
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>