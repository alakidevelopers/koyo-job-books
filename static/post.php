<?php
	$page = "/"; /* ディレクトリルートからのパス */
	$cat = "top"; /* 基本的には、ファイル名orフォルダ名を記載（トップページは"top"で） */
	$ttl = "xxxxx"; /* titleタグに入れる文言 */
	$desc = "xxxxx"; /* discriptionタグに入れる文言 */
	include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/common.php');
	include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header.php');
?>
<main class="main cat-top page">
	<div class="fv">
		<picture>
			<source srcset="/assets/images/job_books_mainimg_page@2x.png" media="<?php echo $media_pc; ?>"/>
			<img src="/assets/images/job_books_mainimg_page@2x.png" /><!-- それ以外で表示 -->
		</picture>
		<h1 class="fv-logo" itemscope itemtype="http://schema.org/Organization">
			<a itemprop="url" href="/"><img itemprop="logo" src="/assets/images/job_books_logo.svg" /></a>
		</h1>
	</div>
	<nav class="breadcrumb">
		<ol class="d-flex" itemscope itemtype="http://schema.org/BreadcrumbList">
			<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
				<a itemprop="item" href="https://example.com/arts">
					<span itemprop="name"><i class="fas fa-home"></i>トップ<i class="fas fa-chevron-right arrow"></i></span></a>
				<meta itemprop="position" content="1" />
			</li>
			<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
				<a itemprop="item" href="https://example.com/arts">
					<span itemprop="name">お仕事図鑑<i class="fas fa-chevron-right arrow"></i></span></a>
				<meta itemprop="position" content="2" />
			</li>
			<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
				<a itemprop="item" href="https://example.com/arts">
					<span itemprop="name">ヴォーカリスト・ミュージシャンのお仕事<i class="fas fa-chevron-right arrow"></i></span></a>
				<meta itemprop="position" content="3" />
			</li>
			<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
				<div itemprop="item" href="https://example.com/arts/books">
					<span itemprop="name">ヴォーカリスト</span>
				</div>
				<meta itemprop="position" content="4" />
			</li>
		</ol>
	</nav><!-- .breadcrumb -->
	<article>
		<section class="sec page-head pt-0 pb-0 bg-gray">
			<div class="sec-body">
				<div class="card card-wide mb-0">
					<picture>
						<source srcset="/assets/images/job_01_mainimg@2x.png" media="<?php echo $media_pc; ?>"/>
						<img src="/assets/images/job_01_mainimg@2x.png" /><!-- それ以外で表示 -->
					</picture>
					<div class="card-body bg-vm position-relative">
						<h2 class="card-title color-white mb-0 layer">
							<span class="tag mb-2">ヴォーカリスト・ミュージシャンのお仕事</span><br>
							ヴォーカリスト
						</h2>
					</div>
					<div class="acordion inner layer bg-vm">
						<h3 class="acordion-ttl">目次</h3>
						<div class="acordion-cont">
							<ul>
								<li>ヴォーカリストとは？</li>
								<li>ヴォーカリストとは？</li>
								<li>ヴォーカリストとは？</li>
								<li>ヴォーカリストとは？</li>
								<li>ヴォーカリストとは？</li>
							</ul>
						</div>
					</div>
				</div><!-- .card -->
			</div><!-- .sec-body -->
		</section><!-- .sec -->
		<section class="sec pt-0 pb-0 bg-gray detail detail-vm">
			<div class="detail-cont">
				<div class="inner">
					<h2 class="detail-ttl" data-color="vm">ヴォーカリストとは？</h2>
					<div class="detail-txt">
						<p>ヴォーカリストとは、コンサート・ライヴ・テレビなどで歌を歌う人のことを指し、グループを組まずに一人で活動します。</p>
						<p>歌声で多くの人々を癒したり、勇気づけたりすることができる、大変やりがいのある仕事です。</p>
						<p>ヴォーカリストにもジャンルがあり、ポップ歌手、ロック歌手、アイドル歌手、演歌歌手など様々です。</p>
						<h3>中見出し</h3>
						<ul>
							<li>コンサートを中心に生の歌を届けることに重点をおいて活動する人</li>
							<li>レコーディングを行いCDなどの音楽作品を作り発信していく人</li>
							<li>企業などの意向が入ったCMソングをはじめドラマやアニメなどの主題歌を歌う人</li>
						</ul>
						<p>このように、ヴォーカリストの個性によって仕事の方向性は異なります。</p>
						<p>たくさんの人に夢を与えることができるため、職業としてのヴォーカリストの人気は高く、活躍できるのは一握りと言われています。</p>
						<p>ですが、大好きな歌を通して自分を表現でき、自分の歌声で世界中の人々に音楽のすばらしさや感動を伝えることができるのは、ヴォーカリストの醍醐味です。</p>
					</div>
				</div>
			</div>
			<div class="detail-cont">
				<div class="inner">
					<h3 class="detail-ttl" data-color="vm">ヴォーカリストとは？</h3>
					<div class="detail-txt">
						<p>ヴォーカリストとは、コンサート・ライヴ・テレビなどで歌を歌う人のことを指し、グループを組まずに一人で活動します。</p>
						<p>歌声で多くの人々を癒したり、勇気づけたりすることができる、大変やりがいのある仕事です。</p>
						<p>ヴォーカリストにもジャンルがあり、ポップ歌手、ロック歌手、アイドル歌手、演歌歌手など様々です。</p>
						<p>〇ヴォーカリストの活動スタイル<br>
						・コンサートを中心に生の歌を届けることに重点をおいて活動する人<br>
						・レコーディングを行いCDなどの音楽作品を作り発信していく人<br>
						・企業などの意向が入ったCMソングをはじめドラマやアニメなどの主題歌を歌う人</p>
						<p>このように、ヴォーカリストの個性によって仕事の方向性は異なります。</p>
						<p>たくさんの人に夢を与えることができるため、職業としてのヴォーカリストの人気は高く、活躍できるのは一握りと言われています。</p>
						<p>ですが、大好きな歌を通して自分を表現でき、自分の歌声で世界中の人々に音楽のすばらしさや感動を伝えることができるのは、ヴォーカリストの醍醐味です。</p>
					</div>
				</div>
			</div>
			<div class="detail-cont">
				<div class="inner">
					<h3 class="detail-ttl" data-color="vm">ヴォーカリストとは？</h3>
					<div class="detail-txt">
						<p>ヴォーカリストとは、コンサート・ライヴ・テレビなどで歌を歌う人のことを指し、グループを組まずに一人で活動します。</p>
						<p>歌声で多くの人々を癒したり、勇気づけたりすることができる、大変やりがいのある仕事です。</p>
						<p>ヴォーカリストにもジャンルがあり、ポップ歌手、ロック歌手、アイドル歌手、演歌歌手など様々です。</p>
						<p>〇ヴォーカリストの活動スタイル<br>
						・コンサートを中心に生の歌を届けることに重点をおいて活動する人<br>
						・レコーディングを行いCDなどの音楽作品を作り発信していく人<br>
						・企業などの意向が入ったCMソングをはじめドラマやアニメなどの主題歌を歌う人</p>
						<p>このように、ヴォーカリストの個性によって仕事の方向性は異なります。</p>
						<p>たくさんの人に夢を与えることができるため、職業としてのヴォーカリストの人気は高く、活躍できるのは一握りと言われています。</p>
						<p>ですが、大好きな歌を通して自分を表現でき、自分の歌声で世界中の人々に音楽のすばらしさや感動を伝えることができるのは、ヴォーカリストの醍醐味です。</p>
					</div>
				</div>
			</div>
		</section><!-- .sec -->
		<section class="sec pt-0 bg-gray">
			<div class="sec-body">
				<div class="card-wrap">
					<div class="card card-wide cat-archive">
						<picture>
							<source srcset="/assets/images/job_01_mainimg@2x.png" media="<?php echo $media_pc; ?>"/>
							<img src="/assets/images/job_01_mainimg@2x.png" /><!-- それ以外で表示 -->
						</picture>
						<div class="card-body position-relative">
							<h3 class="card-title layer color-vm">ヴォーカリスト・<br>ミュージシャン<span class="color-black fz-16 font-weight-bold">のお仕事</span></h5>
							<figure class="cat-icon"><img src="/assets/images/job_01_icon.svg" /></figure>
							<div class="btn-list layer">
								<a href="#" class="btn btn-large btn-full btn-border-left text-left mt-0" data-color="vm">ヴォーカリスト</a>
								<a href="#" class="btn btn-large btn-full btn-border-left text-left mt-0" data-color="vm">ヴォーカリスト</a>
								<a href="#" class="btn btn-large btn-full btn-border-left text-left mt-0" data-color="vm">ヴォーカリスト</a>
								<a href="#" class="btn btn-large btn-full btn-border-left text-left mt-0" data-color="vm">ヴォーカリスト</a>
							</div>
							<div class="text-center">
								<a href="#" class="btn btn-line" data-color="vm">一覧を見る</a>
							</div>
						</div>
					</div><!-- .card -->
					<div class="card inner cat-archive">
						<div class="card-body position-relative">
							<h3 class="card-title layer color-black p-0">関連分野<span class="color-black fz-16 font-weight-bold">のお仕事</span></h5>
							<div class="btn-list layer">
								<a href="#" class="btn btn-large btn-full btn-border-left text-left mt-0" data-color="vm">ヴォーカリスト</a>
								<a href="#" class="btn btn-large btn-full btn-border-left text-left mt-0" data-color="d">ヴォーカリスト</a>
								<a href="#" class="btn btn-large btn-full btn-border-left text-left mt-0" data-color="vm">ヴォーカリスト</a>
								<a href="#" class="btn btn-large btn-full btn-border-left text-left mt-0" data-color="esm">ヴォーカリスト</a>
							</div>
						</div>
					</div><!-- .card -->
				</div><!-- .card-wrap -->
			</div><!-- .sec-body -->
		</section><!-- .sec -->
		<section class="sec other-archive">
			<div class="inner">
				<div class="sec-ttlarea color-white layer">
					<div class="sec-enttl">Popular jobs</div>
					<h2 class="sec-ttl">高校生が<br>よく見ているお仕事</h2>
				</div><!-- .sec-ttlarea -->
				<div class="card-wrap">
					<div class="card">
						<div class="card-badge"><img src="/assets/images/rank1.svg" alt="1"></div>
						<picture>
							<img src="/assets/images/dammy.png" />
						</picture>
						<div class="card-body">
							<div class="tag-cloud mb-3">
								<div class="tag" data-color="vm">ヴォーカリスト・ミュージシャンのお仕事</div>
							</div>
							<h3 class="card-title">テキストテキストテキストテキストテキストテキスト</h5>
							<div class="card-text sentence">
								<p>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</p>
							</div>
							<a href="#" class="btn mt-5" data-color="vm">ヴォーカリストのお仕事を見る</a>
						</div>
					</div><!-- .card -->
					<div class="card">
						<div class="card-badge"><img src="/assets/images/rank2.svg" alt="2"></div>
						<picture>
							<img src="/assets/images/dammy.png" />
						</picture>
						<div class="card-body">
							<div class="tag-cloud mb-3">
								<div class="tag" data-color="vm">ヴォーカリスト・ミュージシャンのお仕事</div>
							</div>
							<h3 class="card-title">テキストテキストテキストテキストテキストテキスト</h5>
							<div class="card-text sentence">
								<p>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</p>
							</div>
							<a href="#" class="btn mt-5" data-color="vm">ヴォーカリストのお仕事を見る</a>
						</div>
					</div><!-- .card -->
					<div class="card">
						<div class="card-badge"><img src="/assets/images/rank3.svg" alt="3"></div>
						<picture>
							<img src="/assets/images/dammy.png" />
						</picture>
						<div class="card-body">
							<div class="tag-cloud mb-3">
								<div class="tag" data-color="vm">ヴォーカリスト・ミュージシャンのお仕事</div>
							</div>
							<h3 class="card-title">テキストテキストテキストテキストテキストテキスト</h5>
							<div class="card-text sentence">
								<p>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</p>
							</div>
							<a href="#" class="btn mt-5" data-color="vm">ヴォーカリストのお仕事を見る</a>
						</div>
					</div><!-- .card -->
				</div><!-- .card-wrap -->
			</div><!-- .inner -->
		</section><!-- .sec -->
	</article>

	<div class="back-top">
		<picture>
			<source srcset="/assets/images/job_books_mainimg_btnarea@2x.png" media="<?php echo $media_pc; ?>"/>
			<img src="/assets/images/job_books_mainimg_btnarea@2x.png" /><!-- それ以外で表示 -->
		</picture>
		<div class="back-top-btnarea">
			<figure class="back-top-logo layer"><img src="/assets/images/job_books_logomark.svg" /></figure>
			<a class="btn btn-full btn-line" href="/" data-color="white">TOPページに戻る</a>
		</div>
	</div>

</main><!-- .main -->
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>