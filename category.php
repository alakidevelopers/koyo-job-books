<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

$media_pc = "(min-width: 991px)";

$uri = get_theme_file_uri();

$cat = get_the_category();

// 取得した配列から必要な情報を変数に入れる
$cat_name = $cat[0]->cat_name; // カテゴリー名
$cat_slug = $cat[0]->category_nicename; // カテゴリースラッグ
$cat_id = $cat[0]->cat_ID; // カテゴリーID
$cat_initial = get_field( 'initial', 'category_' . $cat_id );
$cat_num = get_field( 'num', 'category_' . $cat_id );

$fileds = array(
	'when'=>array('when_contents','when_image','とは?'),
	'job_contents'=>array('job_contents','job_image','の仕事内容'),
	'rewarding'=>array('rewarding_contents','rewarding_image','のやりがい・楽しさ'),
	'become'=>array('become_contents','become_image','になるにはどうすればいいの？'),
	'qualification'=>array('qualification_contents','qualification_image','になるために必要な能力・資格'),
	'message'=>array('message_contents','message_image','になりたい高校生へのメッセージ'),
);

get_header();
?>

<main class="main cat-top page">
	<div class="fv">
		<picture>
			<source srcset="<?php echo $uri; ?>/static/assets/images/job_books_mainimg_page_pc@2x.png" media="<?php echo $media_pc; ?>"/>
			<img src="<?php echo $uri; ?>/static/assets/images/job_books_mainimg_page@2x.png" /><!-- それ以外で表示 -->
		</picture>
		<h1 class="fv-logo" itemscope itemtype="http://schema.org/Organization">
			<a itemprop="url" href="<?php echo home_url(); ?>"><img itemprop="logo" src="<?php echo $uri; ?>/static/assets/images/job_books_logo.svg" /></a>
		</h1>
	</div>
	<nav class="breadcrumb">
		<ol class="d-flex" itemscope itemtype="http://schema.org/BreadcrumbList">
			<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
				<a itemprop="item" href="https://www.music.ac.jp/">
					<span itemprop="name"><i class="fas fa-home"></i>トップ<i class="fas fa-chevron-right arrow"></i></span></a>
				<meta itemprop="position" content="1" />
			</li>
			<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
				<a itemprop="item" href="<?php echo home_url(); ?>">
					<span itemprop="name">お仕事図鑑<i class="fas fa-chevron-right arrow"></i></span></a>
				<meta itemprop="position" content="2" />
			</li>
			<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
				<div itemprop="item">
					<span itemprop="name"><?php echo $cat_name; ?>のお仕事</span>
				</div>
				<meta itemprop="position" content="3" />
			</li>
		</ol>
	</nav><!-- .breadcrumb -->
	<article>
		<section class="sec page-head pt-0 pb-3 bg-gray">
			<div class="sec-body">
				<div class="card card-wide">
					<picture>
						<img src="<?php echo $uri; ?>/static/assets/images/job_<?php echo $cat_num; ?>_mainimg@2x.png" /><!-- それ以外で表示 -->
					</picture>
					<div class="card-body bg-<?php echo $cat_initial; ?> position-relative">
						<h2 class="card-title color-white mb-0 layer"><?php echo $cat_name; ?><span class="fz-16 font-weight-bold">の<br class="d-none d-md-block">お仕事一覧</span></h2>
						<figure class="cat-icon"><img src="<?php echo $uri; ?>/static/assets/images/job_<?php echo $cat_num; ?>_icon_white.svg" /></figure>
					</div>
				</div><!-- .card -->
			</div><!-- .sec-body -->
		</section><!-- .sec -->
		<section class="sec cat-top-list pt-0 bg-gray">
			<div class="inner">
				<div class="card-wrap row">
					<?php
						$args = array(
							'category_name' => $cat_slug,
							'posts_per_page' => -1
						);
						$the_query = new WP_Query( $args );
						if ( $the_query->have_posts() ) :
						while ( $the_query->have_posts() ) : $the_query->the_post();
					?>
					<div class="card col-lg-4">
						<picture class="link-area">
							<a href="<?php the_permalink(); ?>">
								<?php if (has_post_thumbnail()) { //アイキャッチ画像を設定している場合
									the_post_thumbnail('full');
									} else { //アイキャッチ画像を設定していない場合 ?>
									<img src="<?php echo $uri; ?>/static/assets/images/dammy@2x.png" />
								<?php } ?>
							</a>
						</picture>
						<div class="card-body has-btn">
							<h3 class="card-title"><a href="<?php the_permalink(); ?>" class="color-<?php echo $cat_initial; ?>"><?php echo get_the_title(); ?></a></h3>
							<div class="card-text sentence">
								<p>
									<?php
										echo mb_substr(get_field( 'when_contents' ),0,100)."...";
									?>
								</p>
							</div>
							<a href="<?php the_permalink(); ?>" class="btn mt-5" data-color="<?php echo $cat_initial;?>"><?php echo get_the_title(); ?>のお仕事を見る</a>
						</div>
					</div><!-- .card -->
					<?php
						endwhile;
						endif;
						wp_reset_postdata();
					?>
				</div><!-- .card-wrap -->
			</div><!-- .inner -->
		</section><!-- .sec -->
		<section class="sec pt-0 pb-0 bg-gray other-cat">
			<h2 class="fz-20 font-weight-bold mb-5 inner">その他の分野<span class="fz-16 font-weight-bold">のお仕事</span></h2>
			<ul class="cat-other-list">
				<?php
					$other_cats = get_categories();
					foreach ($other_cats as $other_cat):

					$other_cat_name = $other_cat->cat_name; // カテゴリー名
					$other_cat_slug = $other_cat->category_nicename; // カテゴリースラッグ
					$other_cat_id = $other_cat->cat_ID; // カテゴリーID
					$other_cat_initial = get_field( 'initial', 'category_' . $other_cat_id );
					$other_cat_num = get_field( 'num', 'category_' . $other_cat_id );

					if($other_cat_name != $cat_name):
				?>
				<li data-color="<?php echo $other_cat_initial; ?>">
					<a href="<?php echo home_url('/') . $other_cat_slug; ?>/">
						<figure class="cat-other-icon"><img src="<?php echo $uri; ?>/static/assets/images/job_<?php echo $other_cat_num; ?>_icon_color.svg" alt=""></figure>
						<div class="cat-other-ttl">
							<?php echo $other_cat_name; ?><span>のお仕事</span>
						</div>
					</a>
				</li>
				<?php
					endif;
					endforeach;
				?>
			</ul>
		</section><!-- .sec -->

		<?php get_template_part('populur'); ?>

	</article>

	<div class="back-top">
		<picture>
			<source srcset="<?php echo $uri; ?>/static/assets/images/job_books_mainimg_btnarea_pc@2x.png" media="<?php echo $media_pc; ?>"/>
			<img src="<?php echo $uri; ?>/static/assets/images/job_books_mainimg_btnarea@2x.png" /><!-- それ以外で表示 -->
		</picture>
		<div class="back-top-btnarea">
			<figure class="back-top-logo layer"><img src="<?php echo $uri; ?>/static/assets/images/job_books_logomark.svg" /></figure>
			<a class="btn btn-full btn-line" href="/" data-color="white">TOPページに戻る</a>
		</div>
	</div>

</main><!-- .main -->

<?php
get_footer();
